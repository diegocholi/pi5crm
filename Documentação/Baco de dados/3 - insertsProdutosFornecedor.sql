use crm;
DELETE FROM produtoFornecedor WHERE idUser = 1;

INSERT INTO produtoFornecedor (descricao, numeroDocumento, valorCompra, idFornecedor, idUser) VALUES ('Pasta de dente', 'A2530', '5', 1, 1);
INSERT INTO produtoFornecedor (descricao, numeroDocumento, valorCompra, idFornecedor, idUser) VALUES ('Ração', 'A2530', '15.90', 1, 1);
INSERT INTO produtoFornecedor (descricao, numeroDocumento, valorCompra, idFornecedor, idUser) VALUES ('Fralda', 'A2530', '18.60', 1, 1);
INSERT INTO produtoFornecedor (descricao, numeroDocumento, valorCompra, idFornecedor, idUser) VALUES ('TV', 'A2530', '1999', 1, 1);
INSERT INTO produtoFornecedor (descricao, numeroDocumento, valorCompra, idFornecedor, idUser) VALUES ('Monitor', 'A2530', '7500', 1, 1);
INSERT INTO produtoFornecedor (descricao, numeroDocumento, valorCompra, idFornecedor, idUser) VALUES ('Livro de piadas', 'A2530', '2.5', 1, 1);
INSERT INTO produtoFornecedor (descricao, numeroDocumento, valorCompra, idFornecedor, idUser) VALUES ('Caderno 10 matérias', 'A2530', '17;9', 1, 1);
INSERT INTO produtoFornecedor (descricao, numeroDocumento, valorCompra, idFornecedor, idUser) VALUES ('Esmalte', 'A2530', '5', 1, 1);
INSERT INTO produtoFornecedor (descricao, numeroDocumento, valorCompra, idFornecedor, idUser) VALUES ('Cotonete', 'A2530', '7.89', 1, 1);
INSERT INTO produtoFornecedor (descricao, numeroDocumento, valorCompra, idFornecedor, idUser) VALUES ('Sabonete', 'A2530', '2.50', 1, 1);
INSERT INTO produtoFornecedor (descricao, numeroDocumento, valorCompra, idFornecedor, idUser) VALUES ('Arros', 'A2530', '3.5', 1, 1);
INSERT INTO produtoFornecedor (descricao, numeroDocumento, valorCompra, idFornecedor, idUser) VALUES ('Fejão', 'A2530', '7.8', 1, 1);
INSERT INTO produtoFornecedor (descricao, numeroDocumento, valorCompra, idFornecedor, idUser) VALUES ('Copo', 'A2530', '7.9', 1, 1);
SELECT * FROM produtoFornecedor;