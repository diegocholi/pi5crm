use crm;
-- CAixa
SELECT * FROM caixa;
-- SELECT DASHBOARD
(SELECT "conta" AS tabela, caixa.idCaixa, 
	conta.descricao ,operacao.descOperacao AS operacao, 
    SUM(conta.valor) AS valor, 
    null AS quantidade, 
    null AS idProduto, 
    caixa.`data`
		FROM caixa 
        INNER JOIN conta ON caixa.idPagamentos = conta.idPagamentos 
        INNER JOIN operacao ON operacao.idOperacao = caixa.idOperacao
        WHERE caixa.idUser = 1
        GROUP BY caixa.`data`
        ORDER BY caixa.idCaixa DESC)
        UNION
        (SELECT "estoque" AS tabela, 
        caixa.idCaixa, 
        produtofornecedor.descricao, "Entrada" AS operacao, 
        SUM(estoque.valorUnt) AS valor, 
        caixa.quantidade AS quantidade, 
        estoque.idProduto AS idProduto ,
         caixa.`data`
			FROM caixa 
            INNER JOIN estoque ON estoque.idProduto = caixa.idProduto
            INNER JOIN produtofornecedor ON produtofornecedor.idProdutoFornecedor = estoque.idProdutoFornecedor
            WHERE caixa.idUser = 1
            GROUP BY  STR_TO_DATE(caixa.`data`, "%m/%d/%Y")
            ORDER BY caixa.idCaixa DESC);

-- SELECT DASHBOARDS ESTOQUE
SELECT descricao, quantidade FROM estoque INNER JOIN produtofornecedor ON produtofornecedor.idProdutoFornecedor = estoque.idProdutoFornecedor;

SELECT produtofornecedor.descricao, caixa.quantidade AS quantidade_caixa, 
	estoque.quantidade AS quantidade_estoque, 
    produtofornecedor.valorCompra AS vendas 
    FROM caixa 
	INNER JOIN estoque ON estoque.idProduto = caixa.idProduto 
    INNER JOIN produtofornecedor ON produtofornecedor.idProdutoFornecedor = estoque.idProdutoFornecedor
    WHERE estoque.idUser = 1
    GROUP BY produtofornecedor.descricao
    ORDER BY caixa.quantidade DESC;

-- SELECT CAIXA / ESTOQUE
SELECT * FROM caixa 
	INNER JOIN estoque ON estoque.idProduto = caixa.idProduto
    INNER JOIN produtofornecedor ON produtofornecedor.idProdutoFornecedor = estoque.idProdutoFornecedor
    ORDER BY idCaixa DESC;

SELECT caixa.idCaixa, produtofornecedor.descricao, 'Entrada', estoque.valorUnt FROM caixa 
	INNER JOIN estoque ON estoque.idProduto = caixa.idProduto
    INNER JOIN produtofornecedor ON produtofornecedor.idProdutoFornecedor = estoque.idProdutoFornecedor
    ORDER BY idCaixa DESC;

-- SELECT CAIXA / CONTA
SELECT * FROM caixa 
	INNER JOIN conta ON caixa.idPagamentos = conta.idPagamentos 
    INNER JOIN operacao ON operacao.idOperacao = caixa.idOperacao
    ORDER BY idCaixa DESC;
    
    
-- SELECT CAIXA / CONTA + SELECT CAIXA / ESTOQUE
(SELECT "conta" AS tabela, caixa.idCaixa, conta.descricao ,operacao.descOperacao AS operacao, conta.valor, null AS quantidade, null AS idProduto 
		FROM caixa 
        INNER JOIN conta ON caixa.idPagamentos = conta.idPagamentos 
        INNER JOIN operacao ON operacao.idOperacao = caixa.idOperacao
        WHERE caixa.idUser = 1
        ORDER BY caixa.idCaixa DESC)
        UNION
        (SELECT "estoque" AS tabela, caixa.idCaixa, produtofornecedor.descricao, "Entrada" AS operacao, estoque.valorUnt, caixa.quantidade AS quantidade, estoque.idProduto AS idProduto 
			FROM caixa 
            INNER JOIN estoque ON estoque.idProduto = caixa.idProduto
            INNER JOIN produtofornecedor ON produtofornecedor.idProdutoFornecedor = estoque.idProdutoFornecedor
            WHERE caixa.idUser = 1
            ORDER BY caixa.idCaixa DESC)
        LIMIT 30;    

    
SELECT * FROM caixa ORDER BY idCaixa DESC;

INSERT INTO 
	caixa(valor, quantidade, idUser, idOperacao, idProduto, `data`)
    VALUES(27, 15, 1, 1, 1, STR_TO_DATE(now(), "%Y-%m-%d"));
    
    
SELECT quantidade FROM estoque WHERE idUser = 1 AND idProduto = 3 > 0;

SELECT * FROM conta;
UPDATE conta SET `status` = FALSE WHERE idPagamentos = 1;

SELECT * FROM estoque INNER JOIN produtofornecedor ON estoque.idProdutoFornecedor = produtofornecedor.idProdutoFornecedor;
UPDATE estoque SET quantidade = quantidade - 10
                                WHERE idProduto = 3 AND idUser = 1 AND quantidade > 0;



SELECT IF((SELECT quantidade FROM estoque WHERE idUser = 1 AND idProduto = 2 > 0), 
	(INSERT INTO caixa(valor, quantidade, idUser, idOperacao, idProduto, `data`)
                    VALUES(:valor, :quantidade, :idUser, :idOperacao, :idProduto, STR_TO_DATE(now(), "%Y-%m-%d"))), 
    false);



UPDATE estoque SET quantidade = quantidade - 1 
	WHERE idProduto = 1 AND idUser = 1;

    
SELECT idPagamentos, descricao, valor FROM conta 
                        WHERE idUser = 1 AND `status` = 1
                        ORDER BY idPagamentos DESC;
SELECT * FROM conta WHERE `status` = 1;
INSERT INTO 
	caixa(valor, quantidade, idUser, idOperacao, descricao, `data`)
    VALUES(27, 15, 1, 1, "Produto 1", STR_TO_DATE(now(), "%Y-%m-%d"));

-- INSERT ENTRADA CAIXA ITEM FORNECEDOR
SELECT * FROM operacao;


-- Fornecedor
SELECT * FROM crm.fornecedor order by idFornecedor desc;
UPDATE crm.fornecedor SET nome = 'Diego test', tituloPortador = 'A64554ds',
						contato = '41999555145', taxaJuros = '1' WHERE idUser = 1 AND idFornecedor = 72;

-- TOKEN
DELETE FROM token WHERE idUser = 1;
SELECT * FROM token WHERE idUser = 1;

-- providerItems
SELECT * FROM produtoFornecedor ORDER BY idProdutoFornecedor DESC;

INSERT INTO produtoFornecedor (descricao, numeroDocumento, ValorCompra, idFornecedor) 
	VALUE ("Test Descrição", "A35SS25", "12.5", "189");
    
-- Itens Estoque
SELECT * FROM estoque ORDER BY idProduto DESC;
SELECT * FROM estoque  AS e INNER JOIN produtoFornecedor AS p ON e.idProdutoFornecedor = p.idProdutoFornecedor ORDER BY idProduto DESC;
INSERT INTO estoque (quantidade, valorUnt, idUser, idProdutoFornecedor) VALUES (3, 10, 1, 1);

SELECT e.idProduto, idProduto, descricao, valorUnt, quantidade 
	FROM produtoFornecedor AS p 
    INNER JOIN estoque AS e ON p.idProdutoFornecedor = e.idProdutoFornecedor
    WHERE e.idUser = 1 
	ORDER BY idProduto DESC LIMIT 5;
    
   SELECT e.idProduto, descricao, valorUnt, quantidade 
                       FROM produtoFornecedor AS p 
                       INNER JOIN estoque AS e ON p.idProdutoFornecedor = e.idProdutoFornecedor
                       WHERE e.idUser = 1 AND p.descricao like '%Pasta de dente%'
                       ORDER BY idProduto DESC LIMIT 5;
                       
UPDATE estoque SET valorUnt = 10, quantidade = 10
                            WHERE idUser = 1 AND idProduto = 29;


-- Testes contas
SELECT * FROM conta;

SELECT c.idPagamentos, c.descricao, tc.descMov, c.dataVencimento, c.valor, c.`status` FROM conta AS c INNER JOIN tipoConta as tc 
	ON c.idTipoMov = tc.idTipoMov
	ORDER BY c.idPagamentos DESC;
    
UPDATE conta SET descricao = 'test', dataVencimento = now(), valor = 1.50, `status` = false, idTipoMov = 2
                        WHERE idPagamentos = 47 AND idUser = 1;
SELECT * FROM fornecedor;
DELETE FROM fornecedor WHERE idFornecedor = 37;

-- TESTES Movimentação Bancária
SELECT * FROM movbancaria ORDER BY idBank DESC;
INSERT INTO movbancaria (valor, idUser, idTransl) VALUES (25, 1, 1);

SELECT idBank, valor, descricao FROM movbancaria AS mv INNER JOIN tipotrans AS tp 
            ON mv.idTransl = tp.idTransl
            WHERE idUser = 1
            ORDER BY idBank DESC 
            LIMIT 5;

SELECT idBank, valor, `data`, descricao FROM movbancaria AS mv INNER JOIN tipotrans AS tp 
                ON mv.idTransl = tp.idTransl
                WHERE `idUser` = 1 AND mv.`data` = STR_TO_DATE('2019-12-02', "%Y-%m-%d")
                ORDER BY mv.idBank DESC LIMIT 5;

SELECT * FROM tipotrans;

UPDATE movbancaria SET valor = 50,  idTipoMov = 1 
                        WHERE idTransl = 35 AND idUser = 1;
