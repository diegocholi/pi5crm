use crm;

INSERT INTO estoque (quantidade, valorUnt, idUser, idProdutoFornecedor) VALUES (250, 12.5, 1, 1);
INSERT INTO estoque (quantidade, valorUnt, idUser, idProdutoFornecedor) VALUES (500, 12.5, 1, 2);
INSERT INTO estoque (quantidade, valorUnt, idUser, idProdutoFornecedor) VALUES (780, 12.5, 1, 3);
INSERT INTO estoque (quantidade, valorUnt, idUser, idProdutoFornecedor) VALUES (70, 12.5, 1, 4);
INSERT INTO estoque (quantidade, valorUnt, idUser, idProdutoFornecedor) VALUES (55, 12.5, 1, 5);
INSERT INTO estoque (quantidade, valorUnt, idUser, idProdutoFornecedor) VALUES (28, 12.5, 1, 7);
INSERT INTO estoque (quantidade, valorUnt, idUser, idProdutoFornecedor) VALUES (32, 12.5, 1, 8);
INSERT INTO estoque (quantidade, valorUnt, idUser, idProdutoFornecedor) VALUES (78, 12.5, 1, 9);
INSERT INTO estoque (quantidade, valorUnt, idUser, idProdutoFornecedor) VALUES (10, 12.5, 1, 10);
SELECT * FROM estoque AS e INNER JOIN produtoFornecedor AS p ON e.idProdutoFornecedor = p.idProdutoFornecedor;