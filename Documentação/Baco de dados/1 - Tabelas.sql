DROP DATABASE CRM;
CREATE DATABASE CRM DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE CRM;

/* Tabela: (null) */
CREATE TABLE user (
    idUser Integer not null AUTO_INCREMENT,
    login Varchar(100),
    `password` Varchar(100),
    email Varchar(100),
    cnpj Varchar(100),
    nomeFantasia Varchar(100),
    endereco Varchar(100),
   PRIMARY KEY (idUser)
);

/* Tabela: (null) */
CREATE TABLE Token (
    token Varchar(220) not null,
    idUser Integer not null
);

/* Tabela: (null) */
CREATE TABLE caixa (
    idCaixa Integer not null AUTO_INCREMENT,
    valor VARCHAR(200),
    descricao Char(10),
    quantidade Integer,
    idUser Integer not null,
    idOperacao Integer not null,
    idPagamentos Integer,
    idProduto Integer,
    statusCaixa Boolean,
    data Date,
   PRIMARY KEY (idCaixa)
);

/* Tabela: (null) */
CREATE TABLE tipoConta (
    idTipoMov Integer not null AUTO_INCREMENT,
    descMov Varchar(60),
   PRIMARY KEY (idTipoMov)
);

/* Tabela: (null) */
CREATE TABLE conta (
    idPagamentos Integer not null AUTO_INCREMENT,
    descricao Varchar(100),
    desconto Float,
    juros Float,
    dataVencimento Date,
    valor Float,
    status Boolean,
    idClient Integer,
    idFornecedor Integer,
    idTipoMov Integer not null,
    idUser Integer not null,
   PRIMARY KEY (idPagamentos)
);

/* Tabela: (null) */
CREATE TABLE fornecedor (
    idFornecedor Integer not null AUTO_INCREMENT,
    nome Varchar(100),
    dataRegistro Date,
    tituloPortador Varchar(50),
    contato Varchar(25),
    taxaJuros Float,
    idUser Integer not null,
   PRIMARY KEY (idFornecedor)
);

/* Tabela: (null) */
CREATE TABLE operacao (
    idOperacao Integer not null AUTO_INCREMENT,
    descOperacao Char(10),
   PRIMARY KEY (idOperacao)
);

/* Tabela: (null) */
CREATE TABLE estoque (
    idProduto Integer not null AUTO_INCREMENT,
    quantidade Integer,
    valorUnt Float,
    idUser Integer not null,
    idProdutoFornecedor Integer not null,
   PRIMARY KEY (idProduto)
);

/* Tabela: (null) */
CREATE TABLE movBancaria (
    idBank Integer not null AUTO_INCREMENT,
    valor Float,
    data Date,
    idUser Integer not null,
    idTransl Integer not null,
   PRIMARY KEY (idBank)
);

/* Tabela: (null) */
CREATE TABLE cliente (
    idClient Integer not null AUTO_INCREMENT,
    nome Varchar(100),
    dataRegistro Date,
    numDoc Varchar(100),
    tituloPortador Varchar(100),
    dataVencimento Date,
    taxaJuros Float(10,2),
    idUser Integer not null,
   PRIMARY KEY (idClient)
);

/* Tabela: (null) */
CREATE TABLE produtoFornecedor (
    idProdutoFornecedor Integer not null AUTO_INCREMENT,
    descricao Varchar(200),
    numeroDocumento Varchar(100),
    valorCompra Float(10,2),
    idFornecedor Integer not null,
    idUser Integer not null,
   PRIMARY KEY (idProdutoFornecedor)
);

/* Tabela: (null) */
CREATE TABLE tipoTrans (
    idTransl Integer not null AUTO_INCREMENT,
    descricao Char(10),
   PRIMARY KEY (idTransl)
);

/* Relacionamentos */
ALTER TABLE Token ADD CONSTRAINT FK_Token_3 FOREIGN KEY (idUser) REFERENCES user(idUser) ON DELETE CASCADE;
ALTER TABLE caixa ADD CONSTRAINT FK_caixa_7 FOREIGN KEY (idUser) REFERENCES user(idUser) ON DELETE CASCADE;
ALTER TABLE conta ADD CONSTRAINT FK_conta_20 FOREIGN KEY (idFornecedor) REFERENCES fornecedor(idFornecedor) ON DELETE CASCADE;
ALTER TABLE caixa ADD CONSTRAINT FK_caixa_29 FOREIGN KEY (idOperacao) REFERENCES operacao(idOperacao) ON DELETE CASCADE;
ALTER TABLE caixa ADD CONSTRAINT FK_caixa_31 FOREIGN KEY (idPagamentos) REFERENCES conta(idPagamentos) ON DELETE CASCADE;
ALTER TABLE caixa ADD CONSTRAINT FK_caixa_34 FOREIGN KEY (idProduto) REFERENCES estoque(idProduto) ON DELETE CASCADE;
ALTER TABLE movBancaria ADD CONSTRAINT FK_movBancaria_36 FOREIGN KEY (idUser) REFERENCES user(idUser) ON DELETE CASCADE;
ALTER TABLE conta ADD CONSTRAINT FK_conta_40 FOREIGN KEY (idTipoMov) REFERENCES tipoConta(idTipoMov) ON DELETE CASCADE;
ALTER TABLE conta ADD CONSTRAINT FK_conta_44 FOREIGN KEY (idClient) REFERENCES cliente(idClient) ON DELETE CASCADE;
ALTER TABLE fornecedor ADD CONSTRAINT FK_fornecedor_50 FOREIGN KEY (idUser) REFERENCES user(idUser) ON DELETE CASCADE;
ALTER TABLE estoque ADD CONSTRAINT FK_estoque_51 FOREIGN KEY (idUser) REFERENCES user(idUser) ON DELETE CASCADE;
ALTER TABLE cliente ADD CONSTRAINT FK_cliente_52 FOREIGN KEY (idUser) REFERENCES user(idUser) ON DELETE CASCADE;
ALTER TABLE produtoFornecedor ADD CONSTRAINT FK_produtoFornecedor_55 FOREIGN KEY (idFornecedor) REFERENCES fornecedor(idFornecedor) ON DELETE CASCADE;
ALTER TABLE estoque ADD CONSTRAINT FK_estoque_56 FOREIGN KEY (idProdutoFornecedor) REFERENCES produtoFornecedor(idProdutoFornecedor) ON DELETE CASCADE;
ALTER TABLE produtoFornecedor ADD CONSTRAINT FK_produtoFornecedor_58 FOREIGN KEY (idUser) REFERENCES user(idUser) ON DELETE CASCADE;
ALTER TABLE movBancaria ADD CONSTRAINT FK_movBancaria_61 FOREIGN KEY (idTransl) REFERENCES tipoTrans(idTransl) ON DELETE CASCADE;
ALTER TABLE conta ADD CONSTRAINT FK_conta_63 FOREIGN KEY (idUser) REFERENCES user(idUser) ON DELETE CASCADE;

/* Triggers */


/* Triggers */
-- Inserindo campos default
INSERT INTO tipoconta (descMov) VALUES ("conta a pagar");
INSERT INTO tipoconta (descMov) VALUES ("conta a receber");

INSERT INTO operacao(descOperacao) VALUES("Entrada");
INSERT INTO operacao(descOperacao) VALUES("Saída");

INSERT INTO tipotrans (descricao) VALUE ("Entrada");
INSERT INTO tipotrans (descricao) VALUE ("Saída");

INSERT INTO `user` (login, `password`, email) VALUES('diego','123456','diego@diego.com');
INSERT INTO `user` (login, `password`, email) VALUES('fred','123456','fred@fred.com');

SELECT * FROM `tipoconta`;
SELECT * FROM `user`;

