<?php
// OBS: Palavra reservada do TOKEN: tk
include_once '../configurantion/connectionDatabase.php';
include_once '../configurantion/secret.php';

/*  
 * Valor de vezes que o token é criptografado
 * OBS: Valor máximo suportado pelo banco atualmente 3x
*/
$GLOBALS['valorCriptografia'] = 3;

/**
 * Data de validade do token em dias: 0 = 1 dia
 */
$GLOBALS['validadeToken'] = '1';
function middleware($dateUser)
{
    // Verificação se existe um token
    if (isset($dateUser['tk'])) {
        $tokenUser = $dateUser['tk'];
        if (validateToken($tokenUser)) {
            $idUser = decodeTokenId($tokenUser);
            $conn = getDbConnection();
            // **************************** Buscando dados ****************************
            $quary = 'SELECT * FROM token WHERE idUser = :id';
            $select = $conn->prepare($quary);
            //link, valor a ser buscado
            $select->bindValue(':id', $idUser);
            //Executando quary
            $select->execute();
            if ($select->rowCount()) {
                $resultado = $select->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_CLASS
                if ($resultado[0]['token'] == $tokenUser) {
                    return true;
                }
            }
        }
    }
    return false;
}

/**
 * Médodo que busca o id do usuário no token
 */
function decodeTokenId($token)
{
    $pieces = explode(".", decodeTokenRecive($token));
    return base64_decode($pieces[0]);
}

/**
 * Método que valida o token
 */
function validateToken($token)
{
    try {
        // Lógica que verifica as partes do token
        $piecesToken = explode(".",  decodeTokenRecive($token));
        if (count($piecesToken) == 3) {
            // Lógica que verifica a parte da data e hora
            $piecesDate = explode("às", base64_decode($piecesToken[2]));
            if (count($piecesDate) == 2) {
                // Lógica que verifica as partes da data
                $piecesDate = explode(":", base64_decode($piecesToken[2]));
                if (count($piecesDate) == 3) {
                    // Lógica que verifica as partes da Hora
                    $piecesDate = explode("/", base64_decode($piecesToken[2]));
                    if (count($piecesDate) == 3)
                        // Comparando palavra secreta do token
                        if (compareSecretPass($token))
                            // Comparando a data de validade do token
                            if (compareDateValidateToken($token))
                                return true;
                }
            }
        }
    } catch (Exception $e) {
        return false;
    }

    return false;
}

/**
 * Método que comapra a data de validade do token
 */
function compareDateValidateToken($token)
{
    $dataToken = decodeTokenDateInsert($token);
    $dataToken = explode('às', $dataToken);
    $dataToken = $dataToken[0];
    $dataValidateToken = getDateValidateToken($token);
    // Lógica que compara a a data de criação do token com a data de validade dele
    if (strtotime(convertDateCompare($dataToken)) <= strtotime(convertDateCompare($dataValidateToken)))
        return true;

    return false;
}

/**
 * Método que compara a palavra secreta do sistema
 */
function compareSecretPass($token)
{
    $token = decodeTokenRecive($token);
    $token = explode('.', $token);
    $token = base64_decode($token[1]);
    $secretSystem = getSecret();

    if ($token == $secretSystem) {
        return true;
    }
    return false;
}


/**
 * Método que convert a data em uma data comparável
 */
function convertDateCompare($piecesDate)
{
    $piecesDate = explode("/", $piecesDate);
    return $piecesDate[0] . '-' . $piecesDate[1] . '-' . $piecesDate[2];
}

/**
 * Recene o Token sem menum tratamento e retorna a data de inserção
 */
function getDateValidateToken($token)
{
    $dateValidateTokenValue = '+' . $GLOBALS['validadeToken'] . ' days';
    // Lógica que verifica a validade do token
    $piecesDate = decodeTokenDateInsert($token);
    $piecesDate = explode("às", $piecesDate);
    $piecesDate = explode("/", $piecesDate[0]);
    $piecesDate = $piecesDate[0] . '-' . $piecesDate[1] . '-' . $piecesDate[2];
    return date('d/m/Y', strtotime($dateValidateTokenValue, strtotime($piecesDate)));
}

/**
 * Médodo que busca a data de criação do token
 */
function decodeTokenDateInsert($token)
{
    $pieces = explode(".", decodeTokenRecive($token));
    return base64_decode($pieces[2]);
}


/**
 * Método que gerá o token
 */
function generateToken($userDate)
{
    $token = base64_encode($userDate[0]['idUser']) . '.' . base64_encode(getSecret()) . '.' . base64_encode(date('d/m/Y \à\s H:i:s'));
    for ($i = 0; $i < $GLOBALS['valorCriptografia']; $i++) {
        $token = base64_encode($token);
    }
    // Token ID USER + Palavra secreta + data
    return $token;
}

/**
 * Decodificação inicial do token
 */
function decodeTokenRecive($token)
{
    for ($i = 0; $i < $GLOBALS['valorCriptografia']; $i++) {
        $token = base64_decode($token);
    }
    return $token;
}
