<?php
include_once './controllerBase.php';

class Auth
{
    private $conn;
    function __construct($dateUser)
    {
        $this->conn = getDbConnection();
        // **************************** Buscando dados ****************************
        $quary = 'SELECT * FROM `user` WHERE `login` = :user AND `password` = :passwordUser';
        $select = $this->conn->prepare($quary);
        //link, valor a ser buscado
        $select->bindValue(':user', base64_decode($dateUser['user']));
        $select->bindValue(':passwordUser', base64_decode($dateUser['password']));

        //Executando quary
        $select->execute();
        if ($select->rowCount()) {
            $userDate = $select->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_CLASS
            $idUser = $userDate[0]['idUser'];
            $token = generateToken($userDate);
            $this->setTokenUser($token, $idUser);
            echo json_encode($token, JSON_PRETTY_PRINT);
        } else {
            echo json_encode('401: Unauthorized', JSON_PRETTY_PRINT);
        }
    }

    function setTokenUser($token, $idUser)
    {
        $quary = 'SELECT * FROM `token` WHERE `idUser` = :idUser';
        $select = $this->conn->prepare($quary);
        //link, valor a ser buscado
        $select->bindValue(':idUser', $idUser);

        //Executando quary
        $select->execute();
        // Validando se existe token cadastrado se estiver faz o update do novo token no mesmo campo do banco
        if ($select->rowCount()) {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $this->conn->prepare('UPDATE token SET token = :token WHERE idUser = :idUser');
            $stmt->execute(array(
                ':idUser' => $idUser,
                ':token' => $token
            ));
        } else { // Se não existir insere no banco 
            $quary = 'INSERT INTO token (token, idUser) VALUES(:token, :idUser)';
            $insert = $this->conn->prepare($quary);
            $insert->bindParam(':token', $token, PDO::PARAM_STR, 100);
            $insert->bindParam(':idUser', $idUser, PDO::PARAM_INT);
            $insert->execute();
        }
    }
}

if (isset($_GET['dateUser']))
    new Auth($_GET['dateUser']);
