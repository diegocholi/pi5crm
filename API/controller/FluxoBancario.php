<?php
include_once 'controllerBase.php';

class FluxoBancario extends ConexaoBase
{

    private $idUser;

    function insertFluxoBancario($dadosInput)
    {
        // Definir UTF-8 NOS SELECT
        $this->conn->exec("SET NAMES 'utf8';");
        //Decodificação dos tokens
        $this->idUser = decodeTokenId($dadosInput['tk']);
        try {
            $quary = 'INSERT INTO movbancaria (valor, `data`, idUser, idTransl) VALUES (:valor, now(), :idUser, :idTransl)';
            $insert = $this->conn->prepare($quary);
            $insert->bindValue(':valor', $dadosInput['body']['valor']);
            $insert->bindValue(':idTransl', $dadosInput['body']['operacao']);
            $insert->bindValue(':idUser', $this->idUser);

            $insert->execute();
            echo get201Mensage();
        } catch (Exception $e) {
            echo get403Mensage();
        }
        unset($this->conn);
    }

    function getTableFluxoBancarioItemsView($getFluxoBancariosItems)
    {
        // Definir UTF-8 NOS SELECT
        $this->conn->exec("SET NAMES 'utf8';");
        $this->idUser = decodeTokenId($getFluxoBancariosItems['tk']);
        // **************************** Buscando dados ****************************
        $quary = 'SELECT idBank, valor, `data`, descricao FROM movbancaria AS mv INNER JOIN tipotrans AS tp 
            ON mv.idTransl = tp.idTransl
            WHERE idUser = :id
            ORDER BY idBank DESC 
            LIMIT 30';
        $select = $this->conn->prepare($quary);
        //link, valor a ser buscado
        $select->bindValue(':id', $this->idUser);
        //Executando quary
        $select->execute();
        if ($select->rowCount()) {
            $storageItems = $select->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_CLASS
            echo json_encode($storageItems, JSON_PRETTY_PRINT);
        } else {
            echo get403Mensage();
        }
        unset($this->conn);
    }

    function deleteFluxo($deleteFluxo)
    {
        $this->idUser = decodeTokenId($deleteFluxo['tk']);
        $this->id = $deleteFluxo['body'];
        echo json_encode($this->id, JSON_PRETTY_PRINT);
        try {
            $stmt = $this->conn->prepare('DELETE FROM movbancaria WHERE idUser = :idUser AND idBank = :idBank');
            $stmt->bindParam(':idBank', $this->id);
            $stmt->bindParam(':idUser', $this->idUser);
            $stmt->execute();
            echo get200Mensage();
        } catch (PDOException $e) {
            echo get403Mensage();
        }
        unset($this->conn);
    }

    function updateFluxo($updateFluxo)
    {
        // Definir UTF-8 NOS SELECT
        $this->conn->exec("SET NAMES 'utf8';");
        $this->idUser = decodeTokenId($updateFluxo['tk']);
        $this->operacao = number_format($updateFluxo['body']['operacao']);
        $this->valor = $updateFluxo['body']['valor'];
        $this->id = number_format($updateFluxo['body']['id']);

        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $this->conn->prepare('UPDATE movbancaria SET valor = :vl,  idTransl = :idTransl 
                        WHERE idBank = :idBank AND idUser = :userId');
            $stmt->execute(array(
                ':idBank' => $this->id,
                ':idTransl' => $this->operacao,
                ':vl' => $this->valor,
                ':userId' => $this->idUser
            ));
            echo get200Mensage();
        } catch (PDOException $e) {
            echo get404Mensage() . ': ' . $e;
        }
        unset($this->conn);
        $this->idUser = null;
        $this->operacao = null;
        $this->valor = null;
        $this->id = null;
    }

    function searchFluxoItem($searchItem)
    {
        // Definir UTF-8 NOS SELECT
        $this->conn->exec("SET NAMES 'utf8';");

        $this->idUser = decodeTokenId($searchItem['tk']);
        $search = $searchItem['body'];

        // Se search não ouver traz todos os dados
        if (!empty($search)) {
            // **************************** Buscando dados ****************************
            $quary = 'SELECT idBank, valor, `data`, descricao FROM movbancaria AS mv INNER JOIN tipotrans AS tp 
                ON mv.idTransl = tp.idTransl
                WHERE `idUser` = :id AND mv.`data` = STR_TO_DATE(:search, "%Y-%m-%d")
                ORDER BY mv.idBank DESC LIMIT 30';
            $select = $this->conn->prepare($quary);
            //link, valor a ser buscado
            $select->bindParam(':id', $this->idUser);
            $select->bindValue(':search', $search);
            //Executando quary
            $select->execute();
            if ($select->rowCount()) {
                $searchFluxoItem = $select->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_CLASS
                echo json_encode($searchFluxoItem, JSON_PRETTY_PRINT);
            } else {
                echo get403Mensage();
            }
        } else {
            // **************************** Buscando dados ****************************
            $quary = 'SELECT idBank, valor, `data`, descricao FROM movbancaria AS mv INNER JOIN tipotrans AS tp 
                        ON mv.idTransl = tp.idTransl
                        ORDER BY mv.idBank DESC
                        LIMIT 30';
            $select = $this->conn->prepare($quary);
            //link, valor a ser buscado
            $select->bindParam(':id', $this->idUser);
            $select->bindValue(':search', $search);
            //Executando quary
            $select->execute();
            if ($select->rowCount()) {
                $searchFluxoItem = $select->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_CLASS
                echo json_encode($searchFluxoItem, JSON_PRETTY_PRINT);
            } else {
                echo get403Mensage();
            }
        }

        unset($this->conn);
    }
}


if (isset($_GET['inserOprBank'])) {
    if (middleware($_GET['inserOprBank'])) {
        $exec = new FluxoBancario();
        $exec->insertFluxoBancario($_GET['inserOprBank']);
    } else
        echo json_encode(get401Mensage(), JSON_PRETTY_PRINT);
}

if (isset($_GET['getTableView'])) {
    if (middleware($_GET['getTableView'])) {
        $exec = new FluxoBancario();
        $exec->getTableFluxoBancarioItemsView($_GET['getTableView']);
    } else
        echo json_encode(get401Mensage(), JSON_PRETTY_PRINT);
}

if (isset($_POST['deleteFluxoItem'])) {
    if (middleware($_POST['deleteFluxoItem'])) {
        $exec = new FluxoBancario();
        $exec->deleteFluxo($_POST['deleteFluxoItem']);
    } else
        echo json_encode(get401Mensage(), JSON_PRETTY_PRINT);
}

if (isset($_POST['updateFluxo'])) {
    if (middleware($_POST['updateFluxo'])) {
        $exec = new FluxoBancario();
        $exec->updateFluxo($_POST['updateFluxo']);
    } else
        echo json_encode(get401Mensage(), JSON_PRETTY_PRINT);
}

if (isset($_GET['searchFluxo'])) {
    if (middleware($_GET['searchFluxo'])) {
        $exec = new FluxoBancario();
        $exec->searchFluxoItem($_GET['searchFluxo']);
    } else
        echo json_encode(get401Mensage(), JSON_PRETTY_PRINT);
}
