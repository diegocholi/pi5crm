<?php
include_once './controllerBase.php';

class ItemProvider extends ConexaoBase
{
    private $idUser;
    private $idProvider;

    function insertItemProvider($providerItem)
    {
        $this->idUser = decodeTokenId($providerItem['tk']);
        // Decodificação dos tokens
        $providerItem = $providerItem['body'];
        try {
            $quary = 'INSERT INTO produtoFornecedor (descricao, numeroDocumento, ValorCompra, idFornecedor, idUser) 
            VALUE (:descriptionItem, :docNumber, :inputValue, :providerId, :idUser)';
            $insert = $this->conn->prepare($quary);
            $insert->bindValue(':descriptionItem', base64_decode($providerItem['description']));
            $insert->bindValue(':docNumber', base64_decode($providerItem['docNumber']));
            $insert->bindValue(':inputValue', base64_decode($providerItem['buyValue']));
            $insert->bindValue(':providerId', base64_decode($providerItem['idFornecedor']));
            $insert->bindValue(':idUser', $this->idUser);
            $insert->execute();
            echo get201Mensage();
        } catch (Exception $e) {
            echo get403Mensage();
        }
        unset($this->conn);
    }

    function getTableProviderItemsView($getItemsProvider)
    {
        // Definir UTF-8 NOS SELECT
        $this->conn->exec("SET NAMES 'utf8';");
        $this->idUser = decodeTokenId($getItemsProvider['tk']);
        $this->idProvider = base64_decode($getItemsProvider['body']);
        // **************************** Buscando dados ****************************
        $quary = 'SELECT idProdutoFornecedor, descricao, numeroDocumento, valorCompra
                FROM `produtoFornecedor` WHERE `idUser` = :id AND idFornecedor = :idFornecedor ORDER BY idProdutoFornecedor DESC LIMIT 30';
        $select = $this->conn->prepare($quary);
        //link, valor a ser buscado
        $select->bindValue(':id', $this->idUser);
        $select->bindValue(':idFornecedor', $this->idProvider);

        //Executando quary
        $select->execute();
        if ($select->rowCount()) {
            $itemsProvider = $select->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_CLASS
            echo json_encode($itemsProvider, JSON_PRETTY_PRINT);
        } else {
            echo get403Mensage();
        }
        unset($this->conn);
    }

    function searchProvider($searchItemProvider)
    {
        // Definir UTF-8 NOS SELECT
        $this->conn->exec("SET NAMES 'utf8';");
        $this->idUser = decodeTokenId($searchItemProvider['tk']);
        $this->idProvider = base64_decode($searchItemProvider['body']['idProvider']);
        $search = base64_decode($searchItemProvider['body']['search']);
        // **************************** Buscando dados ****************************
        $quary = 'SELECT idProdutoFornecedor, descricao, numeroDocumento, valorCompra
                    FROM `produtoFornecedor` WHERE idUser = :idUser  AND idFornecedor = :idProvider AND `descricao` LIKE :descricao ORDER BY idFornecedor DESC LIMIT 30';
        $select = $this->conn->prepare($quary);

        // Condição de seguraça
        $select->bindParam(':idUser', $this->idUser);
        $select->bindParam(':idProvider', $this->idProvider);
        //link, valor a ser buscado
        $select->bindValue(':descricao', '%' . $search  . '%');

        //Executando quary
        $select->execute();
        if ($select->rowCount()) {
            $fornecedoresItens = $select->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_CLASS
            echo json_encode($fornecedoresItens, JSON_PRETTY_PRINT);
        } else {
            echo get403Mensage();
        }
        unset($this->conn);
    }

    function deleteItemProvider($deleteItemProvider)
    {
        $this->idUser = decodeTokenId($deleteItemProvider['tk']);
        $this->idProvider = base64_decode($deleteItemProvider['body']['idFornecedor']);
        $idProdutoFornecedor = base64_decode($deleteItemProvider['body']['idProdutoFornecedor']);
        try {
            $stmt = $this->conn->prepare('DELETE FROM produtoFornecedor WHERE idFornecedor = :idFornecedor AND idUser = :idUser AND idProdutoFornecedor = :idProdutoFornecedor');
            $stmt->bindParam(':idFornecedor', $this->idProvider);
            $stmt->bindParam(':idUser', $this->idUser);
            $stmt->bindParam(':idProdutoFornecedor', $idProdutoFornecedor);
            $stmt->execute();
            echo get200Mensage();
        } catch (PDOException $e) {
            echo get403Mensage();
        }
        unset($this->conn);
    }

    function updateItemProvider($providerDateUpdate)
    {
        $this->idUser = decodeTokenId($providerDateUpdate['tk']);
        $this->idProvider = base64_decode($providerDateUpdate['body']['idProvider']);
        $descricao = base64_decode($providerDateUpdate['body']['descricao']);
        $docNumero = base64_decode($providerDateUpdate['body']['docNumero']);
        $valorCompra = base64_decode($providerDateUpdate['body']['valorCompra']);
        $idItem = base64_decode($providerDateUpdate['body']['idItemProvider']);

        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $this->conn->prepare('UPDATE produtoFornecedor SET descricao = :descricao, numeroDocumento = :numeroDocumento,
                            valorCompra = :valorCompra WHERE idUser = :idUser AND idFornecedor = :idFornecedor AND idProdutoFornecedor = :idProdutoFornecedor');
            $stmt->execute(array(
                ':descricao' => $descricao,
                ':numeroDocumento' => $docNumero,
                ':valorCompra' => $valorCompra,
                ':idUser' => $this->idUser,
                ':idFornecedor' => $this->idProvider,
                ':idProdutoFornecedor' => $this->idProvider,
                'idProdutoFornecedor' => $idItem
            ));
            echo get200Mensage();
        } catch (PDOException $e) {
            echo get404Mensage();
        }
        unset($this->conn);
    }

    function searchAllItemProvider($searchAllItemProvider)
    {
        // Definir UTF-8 NOS SELECT
        $this->conn->exec("SET NAMES 'utf8';");
        $this->idUser = decodeTokenId($searchAllItemProvider['tk']);
        $search = base64_decode($searchAllItemProvider['body']);
        // **************************** Buscando dados ****************************
        $quary = 'SELECT  idProdutoFornecedor ,descricao FROM `produtoFornecedor` WHERE idUser = :idUser  AND `descricao` LIKE :descricao ORDER BY descricao ASC LIMIT 30';
        $select = $this->conn->prepare($quary);

        // Condição de seguraça
        $select->bindParam(':idUser', $this->idUser);
        //link, valor a ser buscado
        $select->bindValue(':descricao', '%' . $search  . '%');

        //Executando quary
        $select->execute();
        if ($select->rowCount()) {
            $fornecedoresItens = $select->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_CLASS
            echo json_encode($fornecedoresItens, JSON_PRETTY_PRINT);
        } else {
            echo get403Mensage();
        }
        unset($this->conn);
    }
}

if (isset($_POST['inputItemProvider'])) {
    if (middleware($_POST['inputItemProvider'])) {
        $exec = new ItemProvider();
        $exec->insertItemProvider($_POST['inputItemProvider']);
    } else {
        echo get401Mensage();
    }
}

if (isset($_GET['getTableView'])) {
    if (middleware($_GET['getTableView'])) {
        $exec = new ItemProvider();
        $exec->getTableProviderItemsView($_GET['getTableView']);
    } else {
        echo get401Mensage();
    }
}

if (isset($_GET['searchItemProvider'])) {
    if (middleware($_GET['searchItemProvider'])) {
        $exec = new ItemProvider();
        $exec->searchProvider($_GET['searchItemProvider']);
    } else {
        echo get401Mensage();
    }
}

if (isset($_POST['deleteItemProvider']) && !empty($_POST['deleteItemProvider'])) {
    if (middleware($_POST['deleteItemProvider'])) {
        $exec = new ItemProvider();
        $exec->deleteItemProvider($_POST['deleteItemProvider']);
    } else {
        echo get401Mensage();
    }
}

if (isset($_POST['updateItemProvider']) && !empty($_POST['updateItemProvider'])) {
    if (middleware($_POST['updateItemProvider'])) {
        $exec = new ItemProvider();
        $exec->updateItemProvider($_POST['updateItemProvider']);
    } else {
        echo get401Mensage();
    }
}

if (isset($_GET['searchAllItemProvider'])) {
    if (middleware($_GET['searchAllItemProvider'])) {
        $exec = new ItemProvider();
        $exec->searchAllItemProvider($_GET['searchAllItemProvider']);
    } else {
        echo get401Mensage();
    }
}
