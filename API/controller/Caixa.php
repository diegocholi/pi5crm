<?php
include_once 'controllerBase.php';
class Caixa extends ConexaoBase
{
    private $idUser;
    private $descricao;
    private $idItemStorage;
    private $valor;

    function insertEntradaCaixaStorage($dadosInput)
    {
        // Definir UTF-8 NOS SELECT
        $this->conn->exec("SET NAMES 'utf8';");
        //Decodificação dos tokens
        $this->idUser = decodeTokenId($dadosInput['tk']);

        // Entrada com itens de estoque
        if ($dadosInput['body']['storageItens'] == 'true' && !empty($dadosInput['body']['selectItemStorage'])) {
            $this->idItemStorage = $dadosInput['body']['selectItemStorage'];
            $this->valor = $dadosInput['body']['valueInputStorage'];
            $this->quantidade = $dadosInput['body']['itensQuantity'];

            // **************************** Buscando dados ****************************
            $quary = 'SELECT quantidade FROM estoque WHERE idUser = :idUser AND idProduto = :idProduto AND quantidade > 0';
            $select = $this->conn->prepare($quary);
            //link, valor a ser buscado
            $select->bindParam(':idUser', $this->idUser);
            $select->bindValue(':idProduto', $this->idItemStorage);
            //Executando quary
            $select->execute();
            if ($select->rowCount()) {
                $quantidadeItemEstoque = $select->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_CLASS
                $quantidadeItemEstoque = $quantidadeItemEstoque[0]['quantidade'];
                if ($quantidadeItemEstoque > 0) {
                    try {
                        $quary = 'INSERT INTO 
                            caixa(valor, quantidade, idUser, idOperacao, idProduto, `data`)
                            VALUES(:valor, :quantidade, :idUser, :idOperacao, :idProduto, STR_TO_DATE(now(), "%Y-%m-%d"))';
                        $insert = $this->conn->prepare($quary);
                        $insert->bindValue(':valor', $this->valor);
                        $insert->bindValue(':quantidade', $this->quantidade);
                        $insert->bindValue(':idUser', $this->idUser);
                        $insert->bindValue(':idOperacao', 1);
                        $insert->bindValue(':idProduto', $this->idItemStorage);
                        $insert->execute();

                        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        $stmt = $this->conn->prepare('UPDATE estoque SET quantidade = quantidade - :quantidade
                                WHERE idProduto = :idProduto AND idUser = :idUser AND quantidade > 0');
                        $stmt->execute(array(
                            ':quantidade' => $this->quantidade,
                            ':idProduto' => $this->idItemStorage,
                            ':idUser' => $this->idUser
                        ));

                        echo get201Mensage();
                    } catch (Exception $e) {
                        echo get403Mensage();
                    }
                }
            } else {
                echo get403Mensage() . ": Item sem quantidade em estoque";
            }
            // Entrada com descrição
        }
        unset($this->conn);
    }

    function insertSaidaCaixa($dadosInput)
    {
        // Definir UTF-8 NOS SELECT
        $this->conn->exec("SET NAMES 'utf8';");
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //Decodificação dos tokens
        $this->idUser = decodeTokenId($dadosInput['tk']);
        try {
            $idPagamentos = $dadosInput['body']['idConta'];
            $quary = 'INSERT INTO 
                caixa(idUser, idOperacao, idPagamentos, `data`)
                VALUES(:idUser, :idOperacao, :idPagamentos, STR_TO_DATE(now(), "%Y-%m-%d"))';
            $insert = $this->conn->prepare($quary);
            $insert->bindValue(':idUser', $this->idUser);
            $insert->bindValue(':idOperacao', 2);
            $insert->bindValue(':idPagamentos', $idPagamentos);
            $insert->execute();

            $stmt = $this->conn->prepare('UPDATE conta SET `status` = FALSE WHERE idPagamentos = :idPagamentos');
            $stmt->execute(array(
                ':idPagamentos' => $idPagamentos
            ));


            echo get201Mensage();
        } catch (Exception $e) {
            echo get403Mensage();
        }
        echo json_encode($dadosInput, JSON_PRETTY_PRINT);
        unset($this->conn);
    }


    function insertEntradaConta($dadosInput)
    {
        // Definir UTF-8 NOS SELECT
        $this->conn->exec("SET NAMES 'utf8';");
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //Decodificação dos tokens
        $this->idUser = decodeTokenId($dadosInput['tk']);
        try {
            $idPagamentos = $dadosInput['body'];
            $quary = 'INSERT INTO 
                        caixa(idUser, idOperacao, idPagamentos, `data`)
                        VALUES(:idUser, :idOperacao, :idPagamentos, STR_TO_DATE(now(), "%Y-%m-%d"))';
            $insert = $this->conn->prepare($quary);
            $insert->bindValue(':idUser', $this->idUser);
            $insert->bindValue(':idOperacao', 1);
            $insert->bindValue(':idPagamentos', $idPagamentos);
            $insert->execute();

            $stmt = $this->conn->prepare('UPDATE conta SET `status` = FALSE WHERE idPagamentos = :idPagamentos');
            $stmt->execute(array(
                ':idPagamentos' => $idPagamentos
            ));


            echo get201Mensage();
        } catch (Exception $e) {
            echo get403Mensage();
        }
        echo json_encode($dadosInput, JSON_PRETTY_PRINT);
        unset($this->conn);
    }

    function getTableCaixaItemsView($getContasItems)
    {
        // Definir UTF-8 NOS SELECT
        $this->conn->exec("SET NAMES 'utf8';");
        $this->idUser = decodeTokenId($getContasItems['tk']);
        // **************************** Buscando dados ****************************
        $quary = '(SELECT "conta" AS tabela, caixa.idCaixa, conta.descricao ,operacao.descOperacao AS operacao, conta.valor, null AS quantidade, null AS idProduto 
		FROM caixa 
        INNER JOIN conta ON caixa.idPagamentos = conta.idPagamentos 
        INNER JOIN operacao ON operacao.idOperacao = caixa.idOperacao
        WHERE caixa.idUser = :id
        ORDER BY caixa.idCaixa DESC)
        UNION
        (SELECT "estoque" AS tabela, caixa.idCaixa, produtofornecedor.descricao, "Entrada" AS operacao, estoque.valorUnt, caixa.quantidade AS quantidade, estoque.idProduto AS idProduto 
			FROM caixa 
            INNER JOIN estoque ON estoque.idProduto = caixa.idProduto
            INNER JOIN produtofornecedor ON produtofornecedor.idProdutoFornecedor = estoque.idProdutoFornecedor
            WHERE caixa.idUser = :id
            ORDER BY caixa.idCaixa DESC)
        LIMIT 30';
        $select = $this->conn->prepare($quary);
        //link, valor a ser buscado
        $select->bindValue(':id', $this->idUser);
        //Executando quary
        $select->execute();
        if ($select->rowCount()) {
            $storageItems = $select->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_CLASS
            echo json_encode($storageItems, JSON_PRETTY_PRINT);
        } else {
            echo get403Mensage();
        }
        unset($this->conn);
    }

    function getDashboardDate($getContasItems)
    {
        // Definir UTF-8 NOS SELECT
        $this->conn->exec("SET NAMES 'utf8';");
        $this->idUser = decodeTokenId($getContasItems['tk']);
        // **************************** Buscando dados ****************************
        $quary = '(SELECT "conta" AS tabela, caixa.idCaixa, 
        conta.descricao ,operacao.descOperacao AS operacao, 
        SUM(conta.valor) AS valor, 
        null AS quantidade, 
        null AS idProduto, 
        caixa.`data`
            FROM caixa 
            INNER JOIN conta ON caixa.idPagamentos = conta.idPagamentos 
            INNER JOIN operacao ON operacao.idOperacao = caixa.idOperacao
            WHERE caixa.idUser = :id
            GROUP BY caixa.`data`
            ORDER BY caixa.idCaixa DESC)
            UNION
            (SELECT "estoque" AS tabela, 
            caixa.idCaixa, 
            produtofornecedor.descricao, "Entrada" AS operacao, 
            SUM(estoque.valorUnt) AS valor, 
            caixa.quantidade AS quantidade, 
            estoque.idProduto AS idProduto ,
             caixa.`data`
                FROM caixa 
                INNER JOIN estoque ON estoque.idProduto = caixa.idProduto
                INNER JOIN produtofornecedor ON produtofornecedor.idProdutoFornecedor = estoque.idProdutoFornecedor
                WHERE caixa.idUser = :id
                GROUP BY  STR_TO_DATE(caixa.`data`, "%m/%d/%Y")
                ORDER BY caixa.idCaixa DESC)';
        $select = $this->conn->prepare($quary);
        //link, valor a ser buscado
        $select->bindValue(':id', $this->idUser);
        //Executando quary
        $select->execute();
        if ($select->rowCount()) {
            $storageItems = $select->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_CLASS
            echo json_encode($storageItems, JSON_PRETTY_PRINT);
        } else {
            echo get403Mensage();
        }
        unset($this->conn);
    }

    function deleteFluxoCaixa($deleteConta)
    {
        $this->idUser = decodeTokenId($deleteConta['tk']);
        $this->id = $deleteConta['body'];
        echo json_encode($this->id, JSON_PRETTY_PRINT);
        try {
            $stmt = $this->conn->prepare('DELETE FROM caixa WHERE idUser = :idUser AND idCaixa = :idCaixa');
            $stmt->bindParam(':idCaixa', $this->id);
            $stmt->bindParam(':idUser', $this->idUser);
            $stmt->execute();
            echo get200Mensage();
        } catch (PDOException $e) {
            echo get403Mensage();
        }
        unset($this->conn);
    }
}

if (isset($_POST['insertEntradaCaixa'])) {
    if (middleware($_POST['insertEntradaCaixa'])) {
        $exec = new Caixa();
        $exec->insertEntradaCaixaStorage($_POST['insertEntradaCaixa']);
    } else {
        echo get401Mensage();
    }
}

if (isset($_POST['insertSaidaCaixa'])) {
    if (middleware($_POST['insertSaidaCaixa'])) {
        $exec = new Caixa();
        $exec->insertSaidaCaixa($_POST['insertSaidaCaixa']);
    } else {
        echo get401Mensage();
    }
}

if (isset($_POST['contaInsertEntradaCaixa'])) {
    if (middleware($_POST['contaInsertEntradaCaixa'])) {
        $exec = new Caixa();
        $exec->insertEntradaConta($_POST['contaInsertEntradaCaixa']);
    } else {
        echo get401Mensage();
    }
}

if (isset($_GET['getTableView'])) {
    if (middleware($_GET['getTableView'])) {
        $exec = new Caixa();
        $exec->getTableCaixaItemsView($_GET['getTableView']);
    } else {
        echo get401Mensage();
    }
}

if (isset($_POST['deleteItemCaixa'])) {
    if (middleware($_POST['deleteItemCaixa'])) {
        $exec = new Caixa();
        $exec->deleteFluxoCaixa($_POST['deleteItemCaixa']);
    } else {
        echo get401Mensage();
    }
}
if (isset($_GET['getValuesDashboard'])) {
    if (middleware($_GET['getValuesDashboard'])) {
        $exec = new Caixa();
        $exec->getDashboardDate($_GET['getValuesDashboard']);
    } else {
        echo get401Mensage();
    }
}
