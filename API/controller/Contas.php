<?php
include_once './controllerBase.php';

class Contas extends ConexaoBase
{
    private $idUser;
    private $id;
    private  $descricao;
    private  $operacao;
    private  $data;
    private  $valor;

    function getTableContasItemsView($getContasItems)
    {
        // Definir UTF-8 NOS SELECT
        $this->conn->exec("SET NAMES 'utf8';");
        $this->idUser = decodeTokenId($getContasItems['tk']);
        // **************************** Buscando dados ****************************
        $quary = 'SELECT c.idPagamentos, c.descricao, tc.descMov, c.dataVencimento, c.valor, c.`status` 
                FROM conta AS c INNER JOIN tipoConta as tc 
                ON c.idTipoMov = tc.idTipoMov
                WHERE `idUser` = :id
                ORDER BY c.idPagamentos DESC LIMIT 30';
        $select = $this->conn->prepare($quary);
        //link, valor a ser buscado
        $select->bindValue(':id', $this->idUser);
        //Executando quary
        $select->execute();
        if ($select->rowCount()) {
            $storageItems = $select->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_CLASS
            echo json_encode($storageItems, JSON_PRETTY_PRINT);
        } else {
            echo get403Mensage();
        }
        unset($this->conn);
    }


    function deleteConta($deleteConta)
    {
        $this->idUser = decodeTokenId($deleteConta['tk']);
        $this->id = $deleteConta['body'];
        echo json_encode($this->id, JSON_PRETTY_PRINT);
        try {
            $stmt = $this->conn->prepare('DELETE FROM conta WHERE idUser = :idUser AND idPagamentos = :idPagamentos');
            $stmt->bindParam(':idPagamentos', $this->id);
            $stmt->bindParam(':idUser', $this->idUser);
            $stmt->execute();
            echo get200Mensage();
        } catch (PDOException $e) {
            echo get403Mensage();
        }
        unset($this->conn);
    }

    function insertContas($dadosInput)
    {
        // Definir UTF-8 NOS SELECT
        $this->conn->exec("SET NAMES 'utf8';");
        //Decodificação dos tokens
        $this->idUser = decodeTokenId($dadosInput['tk']);
        try {
            $quary = 'INSERT INTO conta (descricao, dataVencimento, valor, `status`, idTipoMov, idUser)
            VALUES (:descricao, :dataVencimento, :valor, true, :idTipoMov, :idUser)';
            $insert = $this->conn->prepare($quary);
            $insert->bindValue(':descricao', $dadosInput['body']['descricao']);
            $insert->bindValue(':dataVencimento', $dadosInput['body']['data']);
            $insert->bindValue(':valor', $dadosInput['body']['valor']);
            $insert->bindValue(':idTipoMov', $dadosInput['body']['operacao']);

            $insert->bindValue(':idUser', $this->idUser);

            $insert->execute();
            echo get201Mensage();
        } catch (Exception $e) {
            echo get403Mensage();
        }
        unset($this->conn);
    }

    function updateConta($updateConta)
    {
        // Definir UTF-8 NOS SELECT
        $this->conn->exec("SET NAMES 'utf8';");
        $this->idUser = decodeTokenId($updateConta['tk']);
        $this->id = number_format($updateConta['body']['id']);
        $this->descricao = $updateConta['body']['descricao'];
        $this->operacao = number_format($updateConta['body']['operacao']);
        $this->data = $updateConta['body']['data'];
        $this->valor = $updateConta['body']['valor'];

        // echo json_encode($this->operacao, JSON_PRETTY_PRINT);

        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $this->conn->prepare('UPDATE conta SET descricao = :descricao, dataVencimento = :venc, valor = :vl, `status` = :stts, idTipoMov = :idTipoMov 
                        WHERE idPagamentos = :idPg AND idUser = :userId');
            $stmt->execute(array(
                ':descricao' => $this->descricao,
                ':venc' => $this->data,
                ':vl' => $this->valor,
                ':stts' => true,
                ':idTipoMov' => $this->operacao,
                ':idPg' => $this->id,
                ':userId' => $this->idUser
            ));
            echo get200Mensage();
        } catch (PDOException $e) {
            echo get404Mensage() . ': ' . $e;
        }
        unset($this->conn);
    }

    function searchContaItem($searchItem)
    {
        // Definir UTF-8 NOS SELECT
        $this->conn->exec("SET NAMES 'utf8';");

        $this->idUser = decodeTokenId($searchItem['tk']);
        $search = $searchItem['body'];

        // **************************** Buscando dados ****************************
        $quary = 'SELECT c.idPagamentos, c.descricao, tc.descMov, c.dataVencimento, c.valor, c.`status` 
                FROM conta AS c INNER JOIN tipoConta as tc 
                ON c.idTipoMov = tc.idTipoMov
                WHERE `idUser` = :id AND c.descricao LIKE :search
                ORDER BY c.idPagamentos DESC LIMIT 30';
        $select = $this->conn->prepare($quary);
        //link, valor a ser buscado
        $select->bindParam(':id', $this->idUser);
        $select->bindValue(':search', '%' . $search . '%');
        //Executando quary
        $select->execute();
        if ($select->rowCount()) {
            $storageItems = $select->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_CLASS
            echo json_encode($storageItems, JSON_PRETTY_PRINT);
        } else {
            echo get403Mensage();
        }

        unset($this->conn);
    }

    function getItemContasForCaixa($getItemContasForCaixa)
    {
        // Definir UTF-8 NOS SELECT
        $this->conn->exec("SET NAMES 'utf8';");
        $this->idUser = decodeTokenId($getItemContasForCaixa['tk']);
        if ($getItemContasForCaixa['body'] == "saida") {
            // **************************** Buscando dados ****************************
            $quary = 'SELECT idPagamentos, descricao, valor FROM conta 
                        WHERE idUser = :id AND `status` = 1 AND idTipoMov = 1';
            $select = $this->conn->prepare($quary);
            //link, valor a ser buscado
            $select->bindValue(':id', $this->idUser);
            //Executando quary
            $select->execute();
            if ($select->rowCount()) {
                $storageItems = $select->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_CLASS
                echo json_encode($storageItems, JSON_PRETTY_PRINT);
            } else {
                echo get403Mensage();
            }
        } else {
            // **************************** Buscando dados ****************************
            $quary = 'SELECT idPagamentos, descricao, valor FROM conta 
                        WHERE idUser = :id AND `status` = 1 AND idTipoMov = 2';
            $select = $this->conn->prepare($quary);
            //link, valor a ser buscado
            $select->bindValue(':id', $this->idUser);
            //Executando quary
            $select->execute();
            if ($select->rowCount()) {
                $storageItems = $select->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_CLASS
                echo json_encode($storageItems, JSON_PRETTY_PRINT);
            } else {
                echo get403Mensage();
            }
        }
        unset($this->conn);
    }
}

if (isset($_GET['getTableView'])) {
    if (middleware($_GET['getTableView'])) {
        $exec = new Contas();
        $exec->getTableContasItemsView($_GET['getTableView']);
    } else
        echo json_encode(get401Mensage(), JSON_PRETTY_PRINT);
}

if (isset($_POST['inputItem'])) {
    if (middleware($_POST['inputItem'])) {
        $exec = new Contas();
        $exec->insertContas($_POST['inputItem']);
    } else
        echo json_encode(get401Mensage(), JSON_PRETTY_PRINT);
}

if (isset($_POST['deleteContaItem'])) {
    if (middleware($_POST['deleteContaItem'])) {
        $exec = new Contas();
        $exec->deleteConta($_POST['deleteContaItem']);
    } else
        echo json_encode(get401Mensage(), JSON_PRETTY_PRINT);
}

if (isset($_POST['updateConta'])) {
    if (middleware($_POST['updateConta'])) {
        $exec = new Contas();
        $exec->updateConta($_POST['updateConta']);
    } else
        echo json_encode(get401Mensage(), JSON_PRETTY_PRINT);
}

if (isset($_GET['buscaConta'])) {
    if (middleware($_GET['buscaConta'])) {
        $exec = new Contas();
        $exec->searchContaItem($_GET['buscaConta']);
    } else
        echo json_encode(get401Mensage(), JSON_PRETTY_PRINT);
}

if (isset($_GET['getItemContasForCaixa'])) {
    if (middleware($_GET['getItemContasForCaixa'])) {
        $exec = new Contas();
        $exec->getItemContasForCaixa($_GET['getItemContasForCaixa']);
    } else
        echo json_encode(get401Mensage(), JSON_PRETTY_PRINT);
}
