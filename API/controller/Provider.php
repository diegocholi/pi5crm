<?php
include_once './controllerBase.php';

class Provider extends ConexaoBase
{
    private $idUser;

    function insertProvider($fornecedor)
    {
        //Decodificação dos tokens
        $this->idUser = decodeTokenId($fornecedor['tk']);

        try {
            $quary = 'INSERT INTO fornecedor (nome, dataRegistro, tituloPortador, contato, taxaJuros, idUser)
                 VALUES(:nome, :dataRegistro, :tituloPortador, :phone, :taxaJuros, :idUser)';
            $insert = $this->conn->prepare($quary);
            $insert->bindValue(':nome', base64_decode($fornecedor['name']));
            $insert->bindValue(':dataRegistro', date('Y/m/d'));
            $insert->bindValue(':tituloPortador', base64_decode($fornecedor['title']));
            $insert->bindValue(':phone', base64_decode($fornecedor['phone']));
            $insert->bindValue(':taxaJuros', base64_decode($fornecedor['percent']));
            $insert->bindValue(':idUser', $this->idUser);
            $insert->execute();
            echo get201Mensage();
        } catch (Exception $e) {
            echo get403Mensage();
        }
        unset($this->conn);
    }

    function getTableProviderView($getFornecedor)
    {
        // Definir UTF-8 NOS SELECT
        $this->conn->exec("SET NAMES 'utf8';");
        $this->idUser = decodeTokenId($getFornecedor['tk']);
        // **************************** Buscando dados ****************************
        $quary = 'SELECT idFornecedor, nome, dataRegistro, tituloPortador, taxaJuros, contato
                FROM `fornecedor` WHERE `idUser` = :id ORDER BY idFornecedor DESC LIMIT 30';
        $select = $this->conn->prepare($quary);
        //link, valor a ser buscado
        $select->bindValue(':id', $this->idUser);

        //Executando quary
        $select->execute();
        if ($select->rowCount()) {
            $fornecedores = $select->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_CLASS
            echo json_encode($fornecedores, JSON_PRETTY_PRINT);
        } else {
            echo get403Mensage();
        }
        unset($this->conn);
    }

    function deleteProvider($idProvider, $token)
    {
        $this->idUser = decodeTokenId($token);
        try {
            $stmt = $this->conn->prepare('DELETE FROM fornecedor WHERE idFornecedor = :id AND idUser = :idUser');
            $stmt->bindParam(':id', $idProvider);
            $stmt->bindParam(':idUser', $this->idUser);
            $stmt->execute();
            echo get200Mensage();
        } catch (PDOException $e) {
            echo get403Mensage();
        }
        unset($this->conn);
    }

    function searchProvider($serarch, $token)
    {
        // Definir UTF-8 NOS SELECT
        $this->conn->exec("SET NAMES 'utf8';");
        $this->idUser = decodeTokenId($token);
        // **************************** Buscando dados ****************************
        $quary = 'SELECT idFornecedor, nome, dataRegistro, tituloPortador, taxaJuros, contato
                FROM `fornecedor` WHERE idUser = :idUser  AND `nome` LIKE :nome ORDER BY idFornecedor DESC LIMIT 30';
        $select = $this->conn->prepare($quary);

        // Condição de seguraça
        $select->bindParam(':idUser', $this->idUser);
        //link, valor a ser buscado
        $select->bindValue(':nome', '%' . $serarch . '%');

        //Executando quary
        $select->execute();
        if ($select->rowCount()) {
            $fornecedores = $select->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_CLASS
            echo json_encode($fornecedores, JSON_PRETTY_PRINT);
        } else {
            echo get403Mensage();
        }
        unset($this->conn);
    }

    function updateProvider($providerDateUpdate, $token)
    {
        $this->idUser = decodeTokenId($token);
        try {
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $this->conn->prepare('UPDATE fornecedor SET nome = :nameUpdate, tituloPortador = :titleUpdate,
                                    contato = :contatoUpdate, taxaJuros = :taxUpdate WHERE idUser = :idUser AND idFornecedor = :idFornecedor');
            $stmt->execute(array(
                ':nameUpdate' => base64_decode($providerDateUpdate['name']),
                ':titleUpdate' => base64_decode($providerDateUpdate['title']),
                ':contatoUpdate' => base64_decode($providerDateUpdate['phone']),
                ':taxUpdate' => base64_decode($providerDateUpdate['percent']),
                ':idUser' => $this->idUser,
                ':idFornecedor' => base64_decode($providerDateUpdate['idProvider'])
            ));
            echo get200Mensage();
        } catch (PDOException $e) {
            echo get404Mensage();
        }

        unset($this->conn);
    }
}

if (isset($_POST['dataFornecedor'])) {
    if (middleware($_POST['dataFornecedor'])) {
        $fornecedor = $_POST['dataFornecedor'];
        $insertProvider = new Provider();
        $insertProvider->insertProvider($fornecedor);
    } else {
        echo get401Mensage();
    }
}

if (isset($_GET['getTableProviderView'])) {
    if (middleware($_GET['getTableProviderView'])) {
        $getFornecedor = $_GET['getTableProviderView'];
        $iProvider = new Provider();
        $iProvider->getTableProviderView($getFornecedor);
    } else {
        echo  get401Mensage();
    }
}

if (isset($_POST['deleteProvider'])) {
    if (middleware($_POST['deleteProvider'])) {
        $getFornecedor = $_POST['deleteProvider'];
        $iProvider = new Provider();
        $iProvider->deleteProvider(base64_decode($getFornecedor['idProvider']), $_POST['deleteProvider']['tk']);
    } else {
        echo get401Mensage();
    }
}

if (isset($_GET['searchProvider'])) {
    if (middleware($_GET['searchProvider'])) {
        $iProvider = new Provider();
        $iProvider->searchProvider(base64_decode($_GET['searchProvider']['search']), $_GET['searchProvider']['tk']);
    } else {
        echo get401Mensage();
    }
}

if (isset($_POST['updateProvider'])) {
    if (middleware($_POST['updateProvider'])) {
        $iProvider = new Provider();
        $iProvider->updateProvider($_POST['updateProvider']['fornecedor'], $_POST['updateProvider']['tk']);
    } else {
        echo get401Mensage();
    }
}
