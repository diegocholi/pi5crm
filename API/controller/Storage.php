<?php
include_once 'controllerBase.php';

class Storage extends ConexaoBase
{
    function insertItemStorage($inputItem)
    {
        //Decodificação dos tokens
        $this->idUser = decodeTokenId($inputItem['tk']);

        //Decodificação dos tokens
        $this->idUser = decodeTokenId($inputItem['tk']);
        try {
            // **************************** Buscando dados ****************************
            $quary = 'SELECT * FROM estoque AS e WHERE e.idUser = :id AND idProdutoFornecedor = :idProdutoFornecedor LIMIT 1';
            $select = $this->conn->prepare($quary);
            //link, valor a ser buscado
            $select->bindValue(':id', $this->idUser);
            $select->bindValue(':idProdutoFornecedor', base64_decode($inputItem['body']['idProdutoFornecedor']));
            //Executando quary
            $select->execute();
            if ($select->rowCount()) {
                // Lógica caso já existe Item cadastrados
                try {
                    $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    $stmt = $this->conn->prepare('UPDATE estoque SET valorUnt = :valorUnt, quantidade = :quantidade + quantidade
                                    WHERE idUser = :idUser AND idProdutoFornecedor = :idProdutoFornecedor');
                    $stmt->execute(array(
                        ':valorUnt' => base64_decode($inputItem['body']['valorUnt']),
                        ':quantidade' => base64_decode($inputItem['body']['quantidade']),
                        ':idUser' => $this->idUser,
                        ':idProdutoFornecedor' => base64_decode($inputItem['body']['idProdutoFornecedor'])
                    ));
                    echo get200Mensage();
                } catch (PDOException $e) {
                    echo get404Mensage();
                }
            } else {
                $quary = 'INSERT INTO estoque (quantidade, valorUnt, idUser, idProdutoFornecedor) 
                VALUES (:quantidade, :valorUnt, :idUser, :idProdutoFornecedor)';
                $insert = $this->conn->prepare($quary);
                $insert->bindValue(':quantidade', base64_decode($inputItem['body']['quantidade']));
                $insert->bindValue(':valorUnt', base64_decode($inputItem['body']['valorUnt']));
                $insert->bindValue(':idUser', $this->idUser);
                $insert->bindValue(':idProdutoFornecedor', base64_decode($inputItem['body']['idProdutoFornecedor']));
                $insert->execute();
                echo get201Mensage();
            }
        } catch (Exception $e) {
            echo get403Mensage();
        }
        unset($this->conn);
    }

    function getTableStorageItemsView($getStorageItems)
    {
        // Definir UTF-8 NOS SELECT
        $this->conn->exec("SET NAMES 'utf8';");
        $this->idUser = decodeTokenId($getStorageItems['tk']);

        // **************************** Buscando dados ****************************
        $quary = 'SELECT e.idProduto, descricao, valorUnt, quantidade 
                FROM produtoFornecedor AS p 
                INNER JOIN estoque AS e ON p.idProdutoFornecedor = e.idProdutoFornecedor
                WHERE e.idUser = :id 
                ORDER BY idProduto DESC LIMIT 30';
        $select = $this->conn->prepare($quary);
        //link, valor a ser buscado
        $select->bindValue(':id', $this->idUser);
        //Executando quary
        $select->execute();
        if ($select->rowCount()) {
            $storageItems = $select->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_CLASS
            echo json_encode($storageItems, JSON_PRETTY_PRINT);
        } else {
            echo get403Mensage();
        }
        unset($this->conn);
    }

    function searchStorageItem($searchItem)
    {
        // Definir UTF-8 NOS SELECT
        $this->conn->exec("SET NAMES 'utf8';");
        $this->idUser = decodeTokenId($searchItem['tk']);
        $search = base64_decode($searchItem['body']);
        // **************************** Buscando dados ****************************
        $quary = 'SELECT e.idProduto, descricao, valorUnt, quantidade 
                       FROM produtoFornecedor AS p 
                       INNER JOIN estoque AS e ON p.idProdutoFornecedor = e.idProdutoFornecedor
                       WHERE e.idUser = :id AND p.descricao LIKE :search
                       ORDER BY idProduto DESC LIMIT 30';
        $select = $this->conn->prepare($quary);
        //link, valor a ser buscado
        $select->bindParam(':id', $this->idUser);
        $select->bindValue(':search', '%' . $search . '%');
        //Executando quary
        $select->execute();
        if ($select->rowCount()) {
            $storageItems = $select->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_CLASS
            echo json_encode($storageItems, JSON_PRETTY_PRINT);
        } else {
            echo get403Mensage();
        }
        unset($this->conn);
    }

    function deleteStorageItem($deleteStorageItem)
    {
        // echo json_encode($deleteStorageItem, JSON_PRETTY_PRINT);

        $this->idUser = decodeTokenId($deleteStorageItem['tk']);
        $idStorageItem = base64_decode($deleteStorageItem['body']);
        try {
            $stmt = $this->conn->prepare('DELETE FROM estoque WHERE idUser = :idUser AND idProduto = :idStorageItem');
            $stmt->bindParam(':idUser', $this->idUser);
            $stmt->bindParam(':idStorageItem', $idStorageItem);
            $stmt->execute();
            echo get200Mensage();
        } catch (PDOException $e) {
            echo get403Mensage();
        }
        unset($this->conn);
    }


    function updateStorageItem($updateStorageItem)
    {

        $this->idUser = decodeTokenId($updateStorageItem['tk']);
        $idItem = base64_decode($updateStorageItem['body']['idStorageItem']);
        $quantidade = base64_decode($updateStorageItem['body']['quantidade']);
        $valor = base64_decode($updateStorageItem['body']['value']);
        if (!empty($valor)) {
            try {
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $stmt = $this->conn->prepare('UPDATE estoque SET valorUnt = :valorUnt, quantidade = :quantidade
                                WHERE idUser = :idUser AND idProduto = :idProduto');
                $stmt->execute(array(
                    ':valorUnt' => $valor,
                    ':quantidade' => $quantidade,
                    ':idUser' => $this->idUser,
                    ':idProduto' => $idItem
                ));
                echo get200Mensage();
            } catch (PDOException $e) {
                echo get404Mensage();
            }
        } else {
            try {
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $stmt = $this->conn->prepare('UPDATE estoque SET quantidade = quantidade + :quantidade
                                WHERE idUser = :idUser AND idProduto = :idProduto');
                $stmt->execute(array(
                    ':quantidade' => $quantidade,
                    ':idUser' => $this->idUser,
                    ':idProduto' => $idItem
                ));
                echo get200Mensage();
            } catch (PDOException $e) {
                echo get404Mensage();
            }
        }
        unset($this->conn);
    }

    function getItemStorageForCaixa($getItemStorageForCaixa)
    {
        // Definir UTF-8 NOS SELECT
        $this->conn->exec("SET NAMES 'utf8';");
        $this->idUser = decodeTokenId($getItemStorageForCaixa['tk']);

        // **************************** Buscando dados ****************************
        $quary = 'SELECT p.idProdutoFornecedor, descricao, quantidade 
                        FROM produtoFornecedor AS p 
                        INNER JOIN estoque AS e ON p.idProdutoFornecedor = e.idProdutoFornecedor
                        WHERE e.idUser = :id AND e.quantidade > 0';
        $select = $this->conn->prepare($quary);
        //link, valor a ser buscado
        $select->bindValue(':id', $this->idUser);
        //Executando quary
        $select->execute();
        if ($select->rowCount()) {
            $storageItems = $select->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_CLASS
            echo json_encode($storageItems, JSON_PRETTY_PRINT);
        } else {
            echo get403Mensage();
        }
        unset($this->conn);
    }

    function getIDashboardEstoque($getContasItems)
    {
        // Definir UTF-8 NOS SELECT
        $this->conn->exec("SET NAMES 'utf8';");
        $this->idUser = decodeTokenId($getContasItems['tk']);
        // **************************** Buscando dados ****************************
        $quary = 'SELECT produtofornecedor.descricao, caixa.quantidade AS quantidade_caixa, 
                estoque.quantidade AS quantidade_estoque, 
                produtofornecedor.valorCompra AS vendas 
                FROM caixa 
                INNER JOIN estoque ON estoque.idProduto = caixa.idProduto 
                INNER JOIN produtofornecedor ON produtofornecedor.idProdutoFornecedor = estoque.idProdutoFornecedor
                WHERE estoque.idUser = :id
                GROUP BY produtofornecedor.descricao
                ORDER BY caixa.quantidade DESC';
        $select = $this->conn->prepare($quary);
        //link, valor a ser buscado
        $select->bindValue(':id', $this->idUser);
        //Executando quary
        $select->execute();
        if ($select->rowCount()) {
            $storageItems = $select->fetchAll(PDO::FETCH_ASSOC); //PDO::FETCH_CLASS
            echo json_encode($storageItems, JSON_PRETTY_PRINT);
        } else {
            echo get403Mensage();
        }
        unset($this->conn);
    }
}

if (isset($_POST['inputItem']) && !empty($_POST['inputItem'])) {
    if (middleware($_POST['inputItem'])) {
        $exec = new Storage();
        $exec->insertItemStorage($_POST['inputItem']);
    } else {
        echo get401Mensage();
    }
}

if (isset($_GET['getTableView']) && !empty($_GET['getTableView'])) {
    if (middleware($_GET['getTableView'])) {
        $exec = new Storage();
        $exec->getTableStorageItemsView($_GET['getTableView']);
    } else {
        echo get401Mensage();
    }
}

if (isset($_GET['searchStorageItem'])) {
    if (middleware($_GET['searchStorageItem'])) {
        $exec = new Storage();
        $exec->searchStorageItem($_GET['searchStorageItem']);
    } else {
        echo get401Mensage();
    }
}

if (isset($_POST['deleteItem'])) {
    if (middleware($_POST['deleteItem'])) {
        $exec = new Storage();
        $exec->deleteStorageItem($_POST['deleteItem']);
    } else {
        echo get401Mensage();
    }
}
if (isset($_POST['updateStorageItem'])) {
    if (middleware($_POST['updateStorageItem'])) {
        $exec = new Storage();
        $exec->updateStorageItem($_POST['updateStorageItem']);
    } else {
        echo get401Mensage();
    }
}

if (isset($_GET['getItemStorageForCaixa'])) {
    if (middleware($_GET['getItemStorageForCaixa'])) {
        $exec = new Storage();
        $exec->getItemStorageForCaixa($_GET['getItemStorageForCaixa']);
    } else {
        echo get401Mensage();
    }
}

if (isset($_GET['getIDashboardEstoque'])) {
    if (middleware($_GET['getIDashboardEstoque'])) {
        $exec = new Storage();
        $exec->getIDashboardEstoque($_GET['getIDashboardEstoque']);
    } else {
        echo get401Mensage();
    }
}
