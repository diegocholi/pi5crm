<?php
function get200Mensage()
{
    return 'SUCCESS: 200 OK';
}

function get201Mensage()
{
    return 'SUCCESS: 201 Created';
}

function get401Mensage()
{
    return 'ERRO: 401 Unauthorized';
}

function get403Mensage()
{
    return 'ERRO: 403 Forbidden';
}

function get404Mensage()
{
    return 'ERRO: 404 Not Found';
}

function get422Mensage()
{
    return 'ERRO: 422 Unprocessable Entity';
}

function getMensageSuccess()
{
    return 'Success';
}
