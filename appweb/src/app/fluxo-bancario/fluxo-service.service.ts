import { Injectable } from '@angular/core';
import * as $ from 'jquery';
import { serviceAccessRoute, fluxoBancario } from '../configuration/serviceConfig';
import { JqueryTratamentResponseService } from '../service/jquery-tratament-response.service';
import { AuthService } from '../login/authService/auth.service';
import { Subject, Observable } from 'rxjs';
import { FluxoInterfaceTableView } from './fluxo-interface';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class FluxoServiceService {
  private subject = new Subject<any>();
  private static response: any;
  constructor(private jqueryTratament: JqueryTratamentResponseService,
    private auth: AuthService) { }

  getSubject(): Observable<FluxoInterfaceTableView[]> {
    return this.subject.asObservable();
  }

  async inserOprBank(formControlOperacao: number, formControlValor: string) {
    await $.ajax({
      url: serviceAccessRoute + fluxoBancario,
      data: {
        inserOprBank: {
          tk: this.auth.getToken(),
          body: {
            operacao: formControlOperacao,
            valor: formControlValor
          }
        }
      },
      cache: false,
      type: 'GET',
      success: function (response) {
        FluxoServiceService.response = response;
      }
    });
    this.jqueryTratament.validateResponse(FluxoServiceService.response);
    this.getTableViewFluxo();
    FluxoServiceService.response = undefined;
  }

  async getTableViewFluxo() {
    await $.ajax({
      url: serviceAccessRoute + fluxoBancario,
      data: {
        getTableView: {
          tk: this.auth.getToken(),
          body: null
        }
      },
      cache: false,
      type: 'GET',
      success: function (response) {
        FluxoServiceService.response = response;
      }
    });

    if (this.jqueryTratament.validateResponse(FluxoServiceService.response)) {
      FluxoServiceService.response = JSON.parse(FluxoServiceService.response);
      this.subject.next(FluxoServiceService.response);
    }
    FluxoServiceService.response = undefined;
  }

  async deleteFluxoItem(id) {
    await $.ajax({
      url: serviceAccessRoute + fluxoBancario,
      data: {
        deleteFluxoItem: {
          tk: this.auth.getToken(),
          body: id
        }
      },
      cache: false,
      type: 'POST',
      success: function (response) {
        FluxoServiceService.response = response;
      }
    });
    this.jqueryTratament.validateResponse(FluxoServiceService.response);
    console.log(FluxoServiceService.response);
    FluxoServiceService.response = undefined;
  }

  async updateFluxo(formUpdate: FormGroup, id: number) {
    let dadosUpdate = {
      id: id,
      operacao: formUpdate.value.formControlOperacao,
      valor: formUpdate.value.formControlValor
    }
    await $.ajax({
      url: serviceAccessRoute + fluxoBancario,
      data: {
        updateFluxo: {
          tk: this.auth.getToken(),
          body: dadosUpdate
        }
      },
      cache: false,
      type: 'POST',
      success: function (response) {
        FluxoServiceService.response = response;
      }
    });

    this.getTableViewFluxo();
    this.jqueryTratament.validateResponse(FluxoServiceService.response);
    FluxoServiceService.response = undefined;
  }

  async searchFluxo(search: string) {
    await $.ajax({
      url: serviceAccessRoute + fluxoBancario,
      data: {
        searchFluxo: {
          tk: this.auth.getToken(),
          body: search
        }
      },
      cache: false,
      type: 'GET',
      success: function (response) {
        FluxoServiceService.response = response;
      }
    });
    if (this.jqueryTratament.validateResponse(FluxoServiceService.response)) {
      FluxoServiceService.response = JSON.parse(FluxoServiceService.response);
      this.subject.next(FluxoServiceService.response);
    }
    FluxoServiceService.response = undefined;
  }

}
