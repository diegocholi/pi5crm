import { Component, OnInit } from '@angular/core';
import { FluxoInterfaceTableView } from './fluxo-interface';
import { FluxoServiceService } from './fluxo-service.service';
import { ComponenteBase } from '../componente-base';
import * as $ from 'jquery';

@Component({
  selector: 'appweb-fluxo-bancario',
  templateUrl: './fluxo-bancario.component.html',
  styleUrls: ['./fluxo-bancario.component.css']
})
export class FluxoBancarioComponent extends ComponenteBase implements OnInit {
  confirmDelete: boolean = false;
  tableView: FluxoInterfaceTableView[] = [];
  id: Number;
  constructor(private service: FluxoServiceService) {
    super();
  }

  ngOnInit() {
    this.service.getTableViewFluxo();
    this.service.getSubject().subscribe(prams => {
      this.tableView = prams;
    });
  }

  deleteFluxoItem(id) {
    this.id = id;
    this.confirmDelete = true;
  }

  responseConfirmDelete(action: boolean) {
    if (action) {
      this.service.deleteFluxoItem(this.id);
      let arrayPosition = this.getPossitionArrayID(Number(this.id), this.tableView);
      this.tableView.splice(arrayPosition, 1);
      this.confirmDelete = false;
    } else {
      this.confirmDelete = false;
    }
    this.id = undefined;
  }

  searchFluxo() {
    this.service.searchFluxo(String($('#search').val()));
  }

}
