import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FluxoBancarioComponent } from './fluxo-bancario.component';

describe('FluxoBancarioComponent', () => {
  let component: FluxoBancarioComponent;
  let fixture: ComponentFixture<FluxoBancarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FluxoBancarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FluxoBancarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
