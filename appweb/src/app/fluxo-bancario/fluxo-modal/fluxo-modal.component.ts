import { Component, OnInit, Input } from '@angular/core';
import * as $ from 'jquery';
import { FluxoServiceService } from '../fluxo-service.service';

@Component({
  selector: 'appweb-fluxo-modal',
  templateUrl: './fluxo-modal.component.html',
  styleUrls: ['./fluxo-modal.component.css']
})
export class FluxoModalComponent implements OnInit {
  /**
   * @motalTitle Titulo do modal
   */


  @Input()
  motalTitle: string = 'Title Default Modal';

  /**
   * @buttonModalName : Edita o nome do botão do motal
   */
  @Input()
  buttonModalName: string = 'Title Default Button Modal';


  /**
  * @buttonClassCss : Configuração da classe css botstrap
  */
  @Input()
  buttonClassCss = 'btn btn-success';


  constructor(private service: FluxoServiceService) { }

  ngOnInit() {
    $('#formControlOperacao').val('')
  }

  inserOprBank() {
    if ($('#formControlOperacao').val()
      && $('#formControlValor').val()) {
      this.service.inserOprBank(Number($('#formControlOperacao').val()), String($('#formControlValor').val()));
      $('#formControlOperacao').val('');
      $('#formControlValor').val('');
    }
    else
      alert('Preencha todos os campos!');
  }

}
