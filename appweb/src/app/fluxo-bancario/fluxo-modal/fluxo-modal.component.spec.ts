import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FluxoModalComponent } from './fluxo-modal.component';

describe('FluxoModalComponent', () => {
  let component: FluxoModalComponent;
  let fixture: ComponentFixture<FluxoModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FluxoModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FluxoModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
