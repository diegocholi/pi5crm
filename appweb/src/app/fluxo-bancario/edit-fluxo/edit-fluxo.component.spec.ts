import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditFluxoComponent } from './edit-fluxo.component';

describe('EditFluxoComponent', () => {
  let component: EditFluxoComponent;
  let fixture: ComponentFixture<EditFluxoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditFluxoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFluxoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
