import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FluxoServiceService } from '../fluxo-service.service';

@Component({
  selector: 'appweb-edit-fluxo',
  templateUrl: './edit-fluxo.component.html',
  styleUrls: ['./edit-fluxo.component.css']
})
export class EditFluxoComponent implements OnInit {
  formGroupFluxo: FormGroup;
  constructor(private formBuilder: FormBuilder, private activedRoute: ActivatedRoute,
    private service: FluxoServiceService, private router: Router) { }
  operacao: number;
  valor: number;
  id: number;
  ngOnInit() {
    this.activedRoute.params.subscribe(params => {
      if (params.operacao === 'Entrada')
        this.operacao = 1;
      else
        this.operacao = 2;
      this.valor = params.valor;
      this.id = params.id;
      console.log(params.operacao);
    });

    this.formGroupFluxo = this.formBuilder.group({
      formControlOperacao: [this.operacao, Validators.required],
      formControlValor: [this.valor, Validators.required]
    });
  }

  updateFluxo() {
    this.service.updateFluxo(this.formGroupFluxo, this.id);
    this.router.navigate(['/controle-bancario']);
  }

}
