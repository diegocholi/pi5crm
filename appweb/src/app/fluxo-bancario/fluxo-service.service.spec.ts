import { TestBed } from '@angular/core/testing';

import { FluxoServiceService } from './fluxo-service.service';

describe('FluxoServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FluxoServiceService = TestBed.get(FluxoServiceService);
    expect(service).toBeTruthy();
  });
});
