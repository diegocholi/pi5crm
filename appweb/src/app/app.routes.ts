import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AuthGuardService } from './guard/auth.guard.service';
import { ContasComponent } from './contas/contas.component';
import { FluxoBancarioComponent } from './fluxo-bancario/fluxo-bancario.component';
import { EditContaComponent } from './contas/edit-conta/edit-conta.component';
import { EditFluxoComponent } from './fluxo-bancario/edit-fluxo/edit-fluxo.component';

export const ROUTES: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuardService] },
    { path: 'login', component: LoginComponent },
    { path: 'conta-controle', component: ContasComponent, canActivate: [AuthGuardService] },
    { path: 'conta-controle/editar/:id/:descricao/:opr/:data/:valor/:status', component: EditContaComponent, canActivate: [AuthGuardService] },
    { path: 'controle-bancario', component: FluxoBancarioComponent, canActivate: [AuthGuardService] },
    { path: 'controle-bancario/editar/:id/:operacao/:valor', component: EditFluxoComponent, canActivate: [AuthGuardService] }
]
