import { Component } from '@angular/core';
import { AuthService } from './login/authService/auth.service';
import { MensageService } from './service/mensage.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'appweb-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  messages: any[] = [];
  inscriptionAuthService: Subscription;
  inscriptionMessageService: Subscription;
  openMenu: boolean = false;
  constructor(private authService: AuthService,
    private messageService: MensageService) {
    if (authService.getStatusUser())
      this.openMenu = true;
  }

  ngOnInit() {
    this.inscriptionAuthService = this.authService.openMenuEmitter.subscribe(
      (s: boolean) => this.openMenu = s
    );
    // Lógica que implementa o observable para exibir mensagens na tela caso haja algum erro
    this.inscriptionMessageService = this.messageService.getMessage().subscribe(message => {
      if (message)
        this.messages.push(message);
      else
        // clear messages when empty message received
        this.messages = [];
    });
  }

  closeErroLoginUser() {
    this.messageService.clearMessages();
    this.messageService.mensageControl = false;
  }

  ngOnDestroy() {
    this.inscriptionAuthService.unsubscribe();
    this.inscriptionMessageService.unsubscribe();
  }

}
