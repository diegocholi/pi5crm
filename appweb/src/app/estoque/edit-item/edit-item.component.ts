import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EstoqueService } from '../estoque.service';

@Component({
  selector: 'appweb-edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.css']
})

export class EditItemComponent implements OnInit {
  descricao: string = '';
  buttonInsertStorage: boolean = true;
  editStorageItemFormGroup: FormGroup;

  idStorageItem: number = null;
  quantidade: number = null;
  valorUnt: number = null;

  controlButton: any = {
    validatorValue: true,
    validatorAmount: true,
  }

  // Implementação da interface que controla as mensagem de require
  controlMensageValidator: any = {
    validatorValue: false,
    validatorAmount: false,
  }
  constructor(private activatedRoutes: ActivatedRoute, private formBuilder: FormBuilder,
    private estoqueServide: EstoqueService, private router: Router) { }

  ngOnInit() {
    this.activatedRoutes.params.subscribe(params => {
      this.idStorageItem = Number(params.idProduto);
      this.descricao = params.descricao;
      this.valorUnt = Number(params.valorUnt);
      this.quantidade = Number(params.quantidade);
    });

    this.editStorageItemFormGroup = this.formBuilder.group({
      value: [this.valorUnt, Validators.required],
      quantidade: [this.quantidade, Validators.required]
    });
  }

  updateStorageItem() {
    this.estoqueServide.updateStorageItem(this.idStorageItem, this.editStorageItemFormGroup);
    this.router.navigate(['/estoque']);
  }

  /**
  * @getStatusControlButton : Validação se todos os campos estão preenchidos
  */
  getStatusControlButton() {
    if (this.controlButton.validatorValue &&
      this.controlButton.validatorAmount) {
      this.buttonInsertStorage = true;
    } else {
      this.buttonInsertStorage = false;
    }
  }

  /**
   * @validatorValue : Validação do valor 
   */
  validatorValue() {
    if ($('#value').val()) {
      this.controlButton.validatorValue = true;
      this.controlMensageValidator.validatorValue = false;
      $('#value').css('borderColor', '#81DAF5');
    } else {
      this.controlButton.validatorValue = false;
      this.controlMensageValidator.validatorValue = true;
      $('#value').css('borderColor', 'red');
    }
    this.getStatusControlButton();
  }

  /**
   * @validatorAmount : Validação quantidade
   */
  validatorAmount() {
    if ($('#amount').val()) {
      this.controlButton.validatorAmount = true;
      this.controlMensageValidator.validatorAmount = false;
      $('#amount').css('borderColor', '#81DAF5');
    } else {
      this.controlButton.validatorAmount = false;
      this.controlMensageValidator.validatorAmount = true;
      $('#amount').css('borderColor', 'red');
    }

    this.getStatusControlButton();
  }

}
