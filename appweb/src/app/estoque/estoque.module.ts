import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EstoqueComponent } from './estoque.component';
import { RelatorioComponent } from './relatorio/relatorio.component';
import { RouterModule } from '@angular/router';
import { estoqueRoutes } from './estoque.router';
import { ModalAddItemComponent } from './modal-add-item/modal-add-item.component';
import { FornecedorModule } from './fornecedor/fornecedor.module';
import { EstoqueService } from './estoque.service';
import { ReactiveFormsModule } from '@angular/forms';
import { EditItemComponent } from './edit-item/edit-item.component';


@NgModule({
  declarations: [
    EstoqueComponent,
    RelatorioComponent,
    ModalAddItemComponent,
    EditItemComponent
  ],
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FornecedorModule,
    RouterModule.forChild(estoqueRoutes)
  ],
  providers: [
    EstoqueService
  ]
})
export class EstoqueModule { }
