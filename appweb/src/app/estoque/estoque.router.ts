import { Routes } from '@angular/router';
import { EstoqueComponent } from './estoque.component';
import { RelatorioComponent } from './relatorio/relatorio.component';
import { AuthGuardService } from '../guard/auth.guard.service';
import { FornecedorComponent } from './fornecedor/fornecedor.component';
import { EditFornecedorComponent } from './fornecedor/edit-fornecedor/edit-fornecedor.component';
import { ItensFornecedorComponent } from './fornecedor/itens-fornecedor/itens-fornecedor.component';
import { EditItensFornecedorComponent } from './fornecedor/itens-fornecedor/edit-itens-fornecedor/edit-itens-fornecedor.component';
import { EditItemComponent } from './edit-item/edit-item.component';

export const estoqueRoutes: Routes = [
    { path: 'estoque', component: EstoqueComponent, canActivate: [AuthGuardService] },
    { path: 'estoque/edit-item/:idProduto/:quantidade/:descricao/:valorUnt', component: EditItemComponent, canActivate: [AuthGuardService] },
    { path: 'estoque/relatorio', component: RelatorioComponent, canActivate: [AuthGuardService] },
    { path: 'estoque/fornecedor', component: FornecedorComponent, canActivate: [AuthGuardService] },
    { path: 'estoque/fornecedor/edit/:idFornecedor/:nome/:contato/:tituloPortador/:taxaJuros/:dataRegistro', component: EditFornecedorComponent, canActivate: [AuthGuardService] },
    { path: 'estoque/fornecedor/itens/:idFornecedor', component: ItensFornecedorComponent, canActivate: [AuthGuardService] },
    { path: 'estoque/fornecedor/itens-edit/:idProdutoFornecedor/:idFornecedor/:descricao/:numeroDocumento/:valorCompra', component: EditItensFornecedorComponent, canActivate: [AuthGuardService] }
]