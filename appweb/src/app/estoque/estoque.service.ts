import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { serviceAccessRoute, storageItensRoute } from '../configuration/serviceConfig'
import { AuthService } from '../login/authService/auth.service';
import { JqueryTratamentResponseService } from '../service/jquery-tratament-response.service';
import { FormGroup } from '@angular/forms';
import { Base64Service } from '../service/base64.service';
import * as $ from 'jquery';

@Injectable({
  providedIn: 'root'
})
export class EstoqueService {
  private subject = new Subject<any>();
  private static response: any;
  constructor(private auth: AuthService, private jqueryTratament: JqueryTratamentResponseService,
    private base64: Base64Service) { }

  getSubject(): Observable<any> {
    return this.subject.asObservable();
  }

  async getTableView() {
    await $.ajax({
      url: serviceAccessRoute + storageItensRoute,
      data: {
        getTableView: {
          tk: this.auth.getToken(),
          body: null
        }
      },
      cache: false,
      type: 'GET',
      success: function (response) {
        EstoqueService.response = response;
      }
    });

    if (this.jqueryTratament.validateResponse(EstoqueService.response)) {
      EstoqueService.response = JSON.parse(EstoqueService.response);
      this.subject.next(EstoqueService.response);
    }
    EstoqueService.response = null;
  }

  async inputItem(tableView: any) {
    await $.ajax({
      url: serviceAccessRoute + storageItensRoute,
      data: {
        inputItem: {
          tk: this.auth.getToken(),
          body: tableView
        }
      },
      cache: false,
      type: 'POST',
      success: function (response) {
        EstoqueService.response = response;
      }
    });

    this.jqueryTratament.validateResponse(EstoqueService.response);
    this.getTableView();
    EstoqueService.response = null;
  }

  async searchStorageItem(formGroupSearch: FormGroup) {
    await $.ajax({
      url: serviceAccessRoute + storageItensRoute,
      data: {
        searchStorageItem: {
          tk: this.auth.getToken(),
          body: this.base64.encodeBase64(formGroupSearch.value.search)
        }
      },
      cache: false,
      type: 'GET',
      success: function (response) {
        EstoqueService.response = response;
      }
    });

    if (this.jqueryTratament.validateResponse(EstoqueService.response)) {
      EstoqueService.response = JSON.parse(EstoqueService.response);
      this.subject.next(EstoqueService.response);
    }
    EstoqueService.response = null;
  }

  async deleteItem(idProduto: number) {
    await $.ajax({
      url: serviceAccessRoute + storageItensRoute,
      data: {
        deleteItem: {
          tk: this.auth.getToken(),
          body: this.base64.encodeBase64(idProduto)
        }
      },
      cache: false,
      type: 'POST',
      success: function (response) {
        EstoqueService.response = response;
      }
    });

    if (this.jqueryTratament.validateResponse(EstoqueService.response)) {
      EstoqueService.response = JSON.parse(EstoqueService.response);
      this.subject.next(EstoqueService.response);
    }
    EstoqueService.response = null;
  }

  async updateStorageItem(idStorageItem: number, formGroup: FormGroup) {
    await $.ajax({
      url: serviceAccessRoute + storageItensRoute,
      data: {
        updateStorageItem: {
          tk: this.auth.getToken(),
          body: {
            idStorageItem: this.base64.encodeBase64(idStorageItem),
            value: this.base64.encodeBase64(formGroup.value.value),
            quantidade: this.base64.encodeBase64(formGroup.value.quantidade)
          }
        }
      },
      cache: false,
      type: 'POST',
      success: function (response) {
        EstoqueService.response = response;
      }
    });

    this.jqueryTratament.validateResponse(EstoqueService.response);
    this.getTableView();
    EstoqueService.response = null;
  }

  async getItemStorageForCaixa() {
    await $.ajax({
      url: serviceAccessRoute + storageItensRoute,
      data: {
        getItemStorageForCaixa: {
          tk: this.auth.getToken(),
          body: null
        }
      },
      cache: false,
      type: 'GET',
      success: function (response) {
        EstoqueService.response = response;
      }
    });
    if (this.jqueryTratament.validateResponse(EstoqueService.response)) {
      EstoqueService.response = JSON.parse(EstoqueService.response);
      this.subject.next(EstoqueService.response);
    }
    EstoqueService.response = null;
  }

  async updateStorageItemForCaixa(idStorageItem: number, quantidade: number) {
    await $.ajax({
      url: serviceAccessRoute + storageItensRoute,
      data: {
        updateStorageItem: {
          tk: this.auth.getToken(),
          body: {
            idStorageItem: this.base64.encodeBase64(idStorageItem),
            value: this.base64.encodeBase64(undefined),
            quantidade: this.base64.encodeBase64(quantidade)
          }
        }
      },
      cache: false,
      type: 'POST',
      success: function (response) {
        EstoqueService.response = response;
      }
    });

    this.jqueryTratament.validateResponse(EstoqueService.response);
    this.getTableView();
    EstoqueService.response = null;
  }

  async getIDashboardEstoque() {
    await $.ajax({
      url: serviceAccessRoute + storageItensRoute,
      data: {
        getIDashboardEstoque: {
          tk: this.auth.getToken(),
          body: null
        }
      },
      cache: false,
      type: 'GET',
      success: function (response) {
        EstoqueService.response = response;
      }
    });

    if (this.jqueryTratament.validateResponse(EstoqueService.response)) {
      EstoqueService.response = JSON.parse(EstoqueService.response);
      this.subject.next(EstoqueService.response);
    }
    EstoqueService.response = undefined;
  }
}
