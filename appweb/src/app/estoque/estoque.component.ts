import { Component, OnInit } from '@angular/core';
import { StorageTableView } from './estoque.interface';
import { EstoqueService } from './estoque.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ComponenteBase } from '../componente-base';

@Component({
  selector: 'appweb-estoque',
  templateUrl: './estoque.component.html',
  styleUrls: ['./estoque.component.css']
})
export class EstoqueComponent extends ComponenteBase implements OnInit {
  idProduto: number;
  tableView: Array<StorageTableView> = [];
  formGroupSearch: FormGroup;
  confirmDeleteMensage: boolean = false;

  constructor(private estoqueService: EstoqueService, private formBuilder: FormBuilder) {
    super();
  }

  ngOnInit() {
    this.estoqueService.getTableView();
    this.estoqueService.getSubject().subscribe(items => {
      this.tableView = items;
    });
    this.formGroupSearch = this.formBuilder.group({
      search: ['']
    });
  }

  searchStorageItem() {
    this.estoqueService.searchStorageItem(this.formGroupSearch);
  }

  deleteStorageItem(idProduto: number) {
    this.confirmDeleteMensage = true;
    this.idProduto = idProduto;
  }

  responseDeleteMensage(action: boolean) {
    if (action) {
      let arrayPosition = this.getPossitionArrayID(this.idProduto, this.tableView);
      this.tableView.splice(arrayPosition, 1);
      this.confirmDeleteMensage = false;
      this.estoqueService.deleteItem(this.idProduto);
    } else {
      this.confirmDeleteMensage = false;
    }
  }
}
