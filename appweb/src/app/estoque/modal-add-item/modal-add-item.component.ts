import { Component, OnInit, Input, EventEmitter } from '@angular/core';
import * as $ from 'jquery';
import { ItensFornecedorService } from '../fornecedor/itens-fornecedor/itens-fornecedor.service';
import { StorageTableView, StorageInserItems } from '../estoque.interface';
import { EstoqueService } from '../estoque.service';
import { Base64Service } from 'src/app/service/base64.service';

@Component({
  selector: 'appweb-modal-add-item',
  templateUrl: './modal-add-item.component.html',
  styleUrls: ['./modal-add-item.component.css']
})
export class ModalAddItemComponent implements OnInit {
  negativeMensageSearch: boolean = false;
  positiveMensageSearch: boolean = false;
  buttonInsertStorage: boolean = false;
  itemSelect: Array<Object> = []
  tableView: Array<StorageTableView>;
  /**
   * @motalTitle Titulo do modal
   */
  @Input()
  motalTitle: string = 'Title Default Modal';
  /**
   * @buttonModalName : Edita o nome do botão do motal
   */
  @Input()
  buttonModalName: string = 'Title Default Button Modal';
  /**
   * @buttonClassCss : Configuração da classe css botstrap
   */
  @Input()
  buttonClassCss = 'btn btn-success';

  // Implementação da interface que controla as mensagem de require
  controlMensageValidator: any = {
    validatorValue: false,
    validatorAmount: false,
    selectionStorageItem: false
  }

  // Implementação de interface que evita que o botão apareça antes de todas as validações
  controlButton: any = {
    validatorValue: false,
    validatorAmount: false,
    selectionStorageItem: false
  }

  constructor(private itemProviderService: ItensFornecedorService, private storageService: EstoqueService,
    private base64: Base64Service) { }

  ngOnInit() {
    this.itemProviderService.getTableItemProviderObservable().subscribe(itemProvider => {
      if (itemProvider) {
        this.itemSelect = itemProvider;
        if (this.itemSelect.length > 0) {
          this.positiveMensageSearch = true;
          this.negativeMensageSearch = false;
        } else {
          this.negativeMensageSearch = true;
          this.positiveMensageSearch = false;
          this.itemProviderService.clearTableItemProviderObservable();
        }
      }
    });
  }

  searchItemsProvider() {
    this.itemProviderService.searchAllItemProvider(String($('#searchAddItem').val())); // Método retorna um array de objetos com as informações buscadas
  }

  saveItemStorage() {
    let insertItens: StorageInserItems =
    {
      quantidade: this.base64.encodeBase64(Number($('#amount').val())),
      valorUnt: this.base64.encodeBase64(Number($('#value').val())),
      idProdutoFornecedor: this.base64.encodeBase64(Number($('#selectItemStorage').val()))
    }
    this.storageService.inputItem(insertItens);
  }

  /**
   * @getStatusControlButton : Validação se todos os campos estão preenchidos
   */
  getStatusControlButton() {
    if (this.controlButton.selectionStorageItem &&
      this.controlButton.validatorValue &&
      this.controlButton.validatorAmount) {
      this.buttonInsertStorage = true;
    } else {
      this.buttonInsertStorage = false;
    }
  }

  /**
   * @validatorDescription : Validação do campo descrição
   */
  validatorDescription() {
    if ($('#description').val()) {
      $('#description').css('borderColor', '#81DAF5');
      this.controlButton.validatorDescription = true;
      this.controlMensageValidator.validatorDescription = false;
    } else {
      this.controlButton.validatorDescription = false;
      this.controlMensageValidator.validatorDescription = true;
      $('#description').css('borderColor', 'red');
    }
    this.getStatusControlButton();
  }

  /**
   * @validatorValue : Validação do valor 
   */
  validatorValue() {
    if ($('#value').val()) {
      this.controlButton.validatorValue = true;
      this.controlMensageValidator.validatorValue = false;
      $('#value').css('borderColor', '#81DAF5');
    } else {
      this.controlButton.validatorValue = false;
      this.controlMensageValidator.validatorValue = true;
      $('#value').css('borderColor', 'red');
    }
    this.getStatusControlButton();
  }

  /**
   * @validatorAmount : Validação quantidade
   */
  validatorAmount() {
    if ($('#amount').val()) {
      this.controlButton.validatorAmount = true;
      this.controlMensageValidator.validatorAmount = false;
      $('#amount').css('borderColor', '#81DAF5');
    } else {
      this.controlButton.validatorAmount = false;
      this.controlMensageValidator.validatorAmount = true;
      $('#amount').css('borderColor', 'red');
    }

    this.getStatusControlButton();
  }

  /**
   * @selectionStorageItem Validação do campo de seleção item de estoque
   */
  selectionStorageItem() {
    if ($('#selectItemStorage').val()) {
      $('#selectItemStorage').css('borderColor', '#81DAF5');
      this.controlButton.selectionStorageItem = true;
      this.controlMensageValidator.selectionStorageItem = false;
    } else {
      $('#selectItemStorage').css('borderColor', 'red');
      this.controlButton.selectionStorageItem = false;
      this.controlMensageValidator.selectionStorageItem = true;
    }
    this.getStatusControlButton();
  }

}
