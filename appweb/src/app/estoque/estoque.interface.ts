export interface StorageTableView {
    idProduto: number
    descricao: string
    valorUnt: number
    quantidade: number
}

export interface StorageInserItems {
    quantidade: string
    valorUnt: string
    idProdutoFornecedor: string
}