import { Component, OnInit } from '@angular/core';
import { FornecedorService } from '../fornecedor.service';
import { MensageControl, Fornecedor, ButtonControl } from '../fornecedor.modal.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import * as $ from 'jquery';

@Component({
  selector: 'appweb-edit-fornecedor',
  templateUrl: './edit-fornecedor.component.html',
  styleUrls: ['./edit-fornecedor.component.css']
})
export class EditFornecedorComponent implements OnInit {

  constructor(private fornecedorService: FornecedorService, private activateRoute: ActivatedRoute,
    private router: Router) { }
  /**
   * Entradas de dados para edição através do input
   */
  name: string = '';
  percent: string = '';
  phone: string = '';
  title: string = '';
  date: string = '';
  idProvider: number;

  inscription: Subscription;
  buttonDisabledControl: boolean = true;
  mensageTax: boolean = false;
  mensageLengh: boolean = false;

  /**
   * @buttonControlEditInterface Interface que controla a mensagem de Require
   */
  mensageRequireControl: MensageControl = {
    name: false,
    title: false,
    percent: false,
    phone: false
  }

  /**
   * @buttonControlEditInterface Interface que controla o disable do botão
   */
  buttonControlEditInterface: ButtonControl = {
    name: true,
    title: true,
    percent: true,
    phone: true
  }

  fornecedor: Fornecedor = {
    name: '',
    percent: null,
    phone: '',
    title: ''
  };

  ngOnInit() {
    this.inscription = this.activateRoute.params.subscribe((params): any => {
      this.idProvider = params['idFornecedor'];
      this.name = params['nome'];
      this.phone = params['contato'];
      this.title = params['tituloPortador'];
      this.percent = params['taxaJuros'];
      this.date = params['nome'];
    });
  }


  updateProvider() {
    if (this.getStatusbuttonControlEditInterface()) {
      this.fornecedor = {
        name: String($('#formControlNameEdit').val()),
        percent: Number($('#formControlPercentEdit').val()),
        phone: String($('#formControlPhoneEdit').val()),
        title: String($('#formControlTitleEdit').val())
      }

      this.fornecedorService.updateProvider(this.fornecedor, this.idProvider);

      // Setando a tabela com os campos editaveis através do evento observable
      this.fornecedorService.setTableProvider([{
        idFornecedor: this.idProvider,
        nome: String($('#formControlNameEdit').val()),
        contato: String($('#formControlPhoneEdit').val()),
        dataRegistro: this.date,
        taxaJuros: String($('#formControlPercentEdit').val()),
        tituloPortador: String($('#formControlTitleEdit').val())
      }]);
      this.router.navigate(['/estoque/fornecedor']);
    }
  }

  /**
     * @getStatusbuttonControlEditInterface Retorna o status da interface de controle do disable do botão
     */
  getStatusbuttonControlEditInterface(): boolean {
    if (
      this.buttonControlEditInterface.name &&
      this.buttonControlEditInterface.title &&
      this.buttonControlEditInterface.percent &&
      this.buttonControlEditInterface.phone
    ) {
      return true;
    }
    return false;
  }

  /**
   * @buttonDisableControl Método que controla o disabled do botão 
   */
  buttonControlEdit() {
    if (this.getStatusbuttonControlEditInterface()) {
      this.buttonDisabledControl = true;
    } else {
      this.buttonDisabledControl = false;
    }
  }

  /**
  * @formControlNameEdit Controle  de validação do campo nome do fornecedor
  */
  formControlNameEdit() {
    if ($('#formControlNameEdit').val()) {
      this.fornecedor.name = String($('#formControlNameEdit').val());

      // Validação se o nome do fornecedor tem mais de 3 caracteres
      if (this.fornecedor.name.length >= 3) {
        // Campo preenchido
        $('#formControlNameEdit').css('borderColor', '#81DAF5');
        this.mensageRequireControl.name = false; // Retira a mensagem de require do formulário
        this.mensageLengh = false; // Retira mensagem lengh
        /**
         * Define a propriedade de controle de botão como true, 
         * todas as propriedades devem ser true para ativar o botão
        */
        this.buttonControlEditInterface.name = true;
      } else {
        $('#formControlNameEdit').css('borderColor', 'red');
        this.mensageLengh = true; // Coloca mensagem lengh
        this.mensageRequireControl.name = false;

        /**
         * Define a propriedade de controle de botão como true, 
         * todas as propriedades devem ser true para ativar o botão
        */
        this.buttonControlEditInterface.name = false;
      }

    } else {
      $('#formControlNameEdit').css('borderColor', 'red');
      this.mensageRequireControl.name = true; // Coloca a mensagem de require do formulário
      this.mensageLengh = false; // Retira mensage lengh
      /**
       * Define a propriedade de controle de botão como true, 
       * todas as propriedades devem ser true para ativar o botão
      */
      this.buttonControlEditInterface.name = false;
    }
    this.buttonControlEdit();
  }

  /**
   * @formControlTitleEdit Controle  de validação do campo titulo do fornecedor
   */
  formControlTitleEdit() {
    if ($('#formControlTitleEdit').val()) {
      // Campo preenchido
      $('#formControlTitleEdit').css('borderColor', '#81DAF5');
      this.mensageRequireControl.title = false; // Retira a mensagem de require do formulário

      /**
       * Define a propriedade de controle de botão como true, 
       * todas as propriedades devem ser true para ativar o botão
      */
      this.buttonControlEditInterface.title = true;
      this.fornecedor.title = String($('#formControlTitleEdit').val());
    } else {
      $('#formControlTitleEdit').css('borderColor', 'red');
      this.mensageRequireControl.title = true; // Coloca a mensagem de require do formulário

      /**
       * Define a propriedade de controle de botão como true, 
       * todas as propriedades devem ser true para ativar o botão
      */
      this.buttonControlEditInterface.title = false;
    }
    this.buttonControlEdit();
  }

  /**
   * @formControlPercentEdit Controle  de validação do campo juros
   */
  formControlPercentEdit() {
    if ($('#formControlPercentEdit').val()) {
      this.fornecedor.percent = Number($('#formControlPercentEdit').val());

      // Verificação de porcentagem
      if (this.fornecedor.percent >= 0 && this.fornecedor.percent <= 100) {
        // Campo preenchido
        $('#formControlPercentEdit').css('borderColor', '#81DAF5');
        this.mensageRequireControl.percent = false; // Retira a mensagem de require do formulário
        this.mensageTax = false;

        /**
         * Define a propriedade de controle de botão como true, 
         * todas as propriedades devem ser true para ativar o botão
        */
        this.buttonControlEditInterface.percent = true;
      } else {
        $('#formControlPercentEdit').css('borderColor', 'red');
        this.mensageRequireControl.percent = false; // Coloca a mensagem de require do formulário
        this.mensageTax = true;
        /**
         * Define a propriedade de controle de botão como true, 
         * todas as propriedades devem ser true para ativar o botão
        */
        this.buttonControlEditInterface.percent = false;
      }
    } else {
      $('#formControlPercentEdit').css('borderColor', 'red');
      this.mensageRequireControl.percent = true; // Coloca a mensagem de require do formulário
      this.mensageTax = false;
      /**
       * Define a propriedade de controle de botão como true, 
       * todas as propriedades devem ser true para ativar o botão
      */
      this.buttonControlEditInterface.percent = false;
    }
    this.buttonControlEdit();
  }

  formControlPhoneEdit() {
    if ($('#formControlPhoneEdit').val()) {
      // Campo preenchido
      $('#formControlPhoneEdit').css('borderColor', '#81DAF5');
      this.mensageRequireControl.phone = false; // Retira a mensagem de require do formulário

      /**
       * Define a propriedade de controle de botão como true, 
       * todas as propriedades devem ser true para ativar o botão
      */
      this.buttonControlEditInterface.phone = true;
      this.fornecedor.phone = String($('#formControlPhoneEdit').val());
    } else {
      $('#formControlPhoneEdit').css('borderColor', 'red');
      this.mensageRequireControl.phone = true; // Coloca a mensagem de require do formulário

      /**
       * Define a propriedade de controle de botão como true, 
       * todas as propriedades devem ser true para ativar o botão
      */
      this.buttonControlEditInterface.phone = false;
    }
    this.buttonControlEdit();
  }

  resetModal() {
    $('#formControlPercentEdit').css('borderColor', '#81DAF5');
    $('#formControlTitleEdit').css('borderColor', '#81DAF5');
    $('#formControlNameEdit').css('borderColor', '#81DAF5');
    $('#formControlPhoneEdit').css('borderColor', '#81DAF5');

    this.mensageRequireControl.name = false;
    this.mensageRequireControl.percent = false;
    this.mensageRequireControl.phone = false;
    this.mensageRequireControl.title = false;

    this.buttonControlEditInterface.name = true;
    this.buttonControlEditInterface.title = true;
    this.buttonControlEditInterface.percent = true;
    this.buttonControlEditInterface.phone = true;
    this.buttonDisabledControl = true;
  }

  ngOnDestroy() {
    this.inscription.unsubscribe();
  }
}
