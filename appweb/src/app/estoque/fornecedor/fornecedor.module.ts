import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FornecedorComponent } from '../fornecedor/fornecedor.component';
import { ModalAddFornecedorComponent } from '../fornecedor/modal-add-fornecedor/modal-add-fornecedor.component';
import { RouterModule } from '@angular/router';
import { estoqueRoutes } from '../estoque.router';
import { FornecedorService } from './fornecedor.service';
import { Base64Service } from 'src/app/service/base64.service';
import { AuthService } from 'src/app/login/authService/auth.service';
import { ReactiveFormsModule } from '@angular/forms';
import { EditFornecedorComponent } from './edit-fornecedor/edit-fornecedor.component';
import { ItensFornecedorComponent } from './itens-fornecedor/itens-fornecedor.component';
import { ModalAddItensFornecedorComponent } from './itens-fornecedor/modal-add-itens-fornecedor/modal-add-itens-fornecedor.component';
import { ItensFornecedorService } from './itens-fornecedor/itens-fornecedor.service';
import { EditItensFornecedorComponent } from './itens-fornecedor/edit-itens-fornecedor/edit-itens-fornecedor.component';

@NgModule({
  declarations: [
    FornecedorComponent,
    ModalAddFornecedorComponent,
    EditFornecedorComponent,
    ItensFornecedorComponent,
    ModalAddItensFornecedorComponent,
    EditItensFornecedorComponent
  ],
  imports: [
    ReactiveFormsModule,
    CommonModule,
    RouterModule.forChild(estoqueRoutes)
  ],
  providers: [
    FornecedorService,
    AuthService,
    Base64Service,
    ItensFornecedorService
  ]
})
export class FornecedorModule { }
