import { Injectable } from '@angular/core';
import { Fornecedor, TableViewFornecedor } from './fornecedor.modal.interface';
import { Base64Service } from './../../service/base64.service';
import { serviceAccessRoute, fornecedorRoute } from '../../configuration/serviceConfig';
import { AuthService } from '../../login/authService/auth.service';
import * as $ from 'jquery';
import { Subject, Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class FornecedorService {

  private static subject = new Subject<any>();

  /**
   * @statusTokenRedirect Controle o valor do status da resposta do servidor, 
   * caso for 401 será redirecionado através do métorodo redirect401Err()
   */
  private static statusTokenRedirect: boolean = false;

  constructor(private base64: Base64Service, private auth: AuthService) { }

  /**
   * @validateResponse valida a resposta do ajax 
   * @response resposta da requisição ajax
   */
  public static validateResponse(response: any) {
    if (response) {
      switch (response) {
        case 'SUCCESS: 200 OK':
          console.log(response);
          break;
        case 'SUCCESS: 201 Created':
          console.log(response);
          break;
        case 'ERRO: 401 Unauthorized':
          FornecedorService.statusTokenRedirect = true;
          console.log(response);
          break;
        case 'ERRO: 403 Forbidden':
          FornecedorService.subject.next({
            dataRegistro: '',
            idFornecedor: '',
            nome: '',
            taxaJuros: '',
            tituloPortador: '',
            contato: ''
          });
          console.log(response);
          break;
        case 'ERRO: 404 Not Found':
          console.log(response);
          break;
        default:
          // se isGetTableView for true ele atualiza a tabela com as informações do servidor
          response = JSON.parse(response);
          FornecedorService.subject.next({ response });

      }
    }
  }

  getTableObservable(): Observable<any> {
    return FornecedorService.subject.asObservable();
  }

  setTableProvider(tableViewProvider: any) {
    FornecedorService.subject.next({ tableViewProvider });
  }

  clearTableProvider() {
    FornecedorService.subject.next({
      dataRegistro: '',
      idFornecedor: '',
      nome: '',
      taxaJuros: '',
      tituloPortador: '',
      contato: ''
    });
  }

  async insertFornecedor(fornecedor: Fornecedor) {
    await $.ajax({
      url: serviceAccessRoute + fornecedorRoute,
      data: {
        dataFornecedor: {
          tk: this.auth.getToken(),
          name: this.base64.encodeBase64(fornecedor.name),
          title: this.base64.encodeBase64(fornecedor.title),
          percent: this.base64.encodeBase64(fornecedor.percent),
          phone: this.base64.encodeBase64(fornecedor.phone)
        }
      },
      cache: false,
      type: 'POST',
      success: function (response) {
        FornecedorService.validateResponse(response);
      }
    });
    // Redirecionando se o retorno for 401
    this.redirect401Erro();

    // Método chamado para atualizar a lista após a inserção de um item
    this.getTableProviderView();
  }

  async getTableProviderView() {
    await $.ajax({
      url: serviceAccessRoute + fornecedorRoute,
      data: {
        getTableProviderView: {
          tk: this.auth.getToken()
        }
      },
      cache: false,
      type: 'GET',
      success: function (response) {
        FornecedorService.validateResponse(response);
      }
    });
    // Redirecionando se o retorno for 401
    this.redirect401Erro();
  }

  async updateProvider(fornecedorUpdate: Fornecedor, idProvider: number) {
    let fornecedorUpdateEncode = {
      idProvider: this.base64.encodeBase64(idProvider),
      name: this.base64.encodeBase64(fornecedorUpdate.name),
      percent: this.base64.encodeBase64(fornecedorUpdate.percent),
      phone: this.base64.encodeBase64(fornecedorUpdate.phone),
      title: this.base64.encodeBase64(fornecedorUpdate.title)
    }
    await $.ajax({
      url: serviceAccessRoute + fornecedorRoute,
      data: {
        updateProvider: {
          tk: this.auth.getToken(),
          fornecedor: fornecedorUpdateEncode
        }
      },
      cache: false,
      type: 'POST',
      success: function (response) {
        FornecedorService.validateResponse(response);
      }
    });
  }

  async deleteProvider(idDelete: number) {
    await $.ajax({
      url: serviceAccessRoute + fornecedorRoute,
      data: {
        deleteProvider: {
          tk: this.auth.getToken(),
          idProvider: this.base64.encodeBase64(idDelete)
        }
      },
      cache: false,
      type: 'POST',
      success: function (response) {
        FornecedorService.validateResponse(response);
      }
    });

    // Redirecionando se o retorno for 401
    this.redirect401Erro();
  }

  async searchProvider(formSearch: FormGroup) {
    await $.ajax({
      url: serviceAccessRoute + fornecedorRoute,
      data: {
        searchProvider: {
          tk: this.auth.getToken(),
          search: this.base64.encodeBase64(formSearch.value.search)
        }
      },
      cache: false,
      type: 'GET',
      success: function (response) {
        FornecedorService.validateResponse(response);
      }
    });
    // Redirecionando se o retorno for 401
    this.redirect401Erro();
  }

  /**
   *  @redirect401Erro Verificação se usuário tem um token válido com base na resposta do servidor
  */
  redirect401Erro() {
    if (FornecedorService.statusTokenRedirect) {
      this.auth.signOut();
    }
  }
}
