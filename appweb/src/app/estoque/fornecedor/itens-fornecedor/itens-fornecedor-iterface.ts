export interface RequireFieldProvider {
    description: boolean
    docNumber: boolean
    purchaseValue: boolean
}

export interface ButtonControl {
    description: boolean
    docNumber: boolean
    purchaseValue: boolean
}

export interface TableItensProvider {
    idProdutoFornecedor: number
    idFornecedor: Number
    descricao: string
    numeroDocumento: string
    valorCompra: Number
}