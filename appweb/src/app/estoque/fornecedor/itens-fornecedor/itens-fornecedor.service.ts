import { Injectable } from '@angular/core';
import { TableItensProvider } from './itens-fornecedor-iterface';
import { Base64Service } from 'src/app/service/base64.service';
import * as $ from 'jquery';
import { serviceAccessRoute, fornecedorItensRoute } from '../../../configuration/serviceConfig';
import { AuthService } from 'src/app/login/authService/auth.service';
import { JqueryTratamentResponseService } from 'src/app/service/jquery-tratament-response.service';
import { Subject, Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ItensFornecedorService {
  private subject = new Subject<any>();
  private static response: any;

  constructor(private base64: Base64Service, private auth: AuthService, private responseTratament: JqueryTratamentResponseService,
    private router: Router) { }

  getTableItemProviderObservable(): Observable<any> {
    return this.subject.asObservable();
  }

  clearTableItemProviderObservable() {
    this.subject.next();
  }

  async inputItemProvider(inputItems: Array<TableItensProvider>) {
    let idFornecedor: number = Number(inputItems[0].idFornecedor);
    ItensFornecedorService.response = null;
    let inputItemsEncode: any = {
      idFornecedor: this.base64.encodeBase64(inputItems[0].idFornecedor),
      description: this.base64.encodeBase64(inputItems[0].descricao),
      docNumber: this.base64.encodeBase64(inputItems[0].numeroDocumento),
      buyValue: this.base64.encodeBase64(inputItems[0].valorCompra)
    }
    await $.ajax({
      url: serviceAccessRoute + fornecedorItensRoute,
      data: {
        inputItemProvider: {
          tk: this.auth.getToken(),
          body: inputItemsEncode
        }
      },
      cache: false,
      type: 'POST',
      success: function (response: any) {
        if (response) {
          ItensFornecedorService.response = response;
        }
      }
    });

    this.responseTratament.validateResponse(ItensFornecedorService.response);
    this.getTableView(idFornecedor);
  }

  async getTableView(idFornecedor: number) {
    ItensFornecedorService.response = null;
    await $.ajax({
      url: serviceAccessRoute + fornecedorItensRoute,
      data: {
        getTableView: {
          tk: this.auth.getToken(),
          body: this.base64.encodeBase64(idFornecedor)
        }
      },
      type: 'GET',
      cache: false,
      success: function (response: any) {
        if (response) {
          ItensFornecedorService.response = response;
        }
      }
    });
    // Validando resposta do servidor
    if (this.responseTratament.validateResponse(ItensFornecedorService.response)) {
      let response = JSON.parse(ItensFornecedorService.response);
      this.subject.next(response);
    }

  }

  async searchAllItemProvider(search: string) {
    await $.ajax({
      url: serviceAccessRoute + fornecedorItensRoute,
      data: {
        searchAllItemProvider: {
          tk: this.auth.getToken(),
          body: this.base64.encodeBase64(search)
        }
      },
      cache: false,
      type: 'GET',
      success: function (response) {
        if (response)
          ItensFornecedorService.response = response;
      }
    });
    // Validando resposta do servidor
    if (this.responseTratament.validateResponse(ItensFornecedorService.response)) {
      let response = JSON.parse(ItensFornecedorService.response);
      this.subject.next(response);
    } else {
      this.subject.next([]);
    }
  }

  async searchItemProvider(searchItemProviderFormGroup: FormGroup, idProvider: number) {
    let bodyConstruct = {
      search: this.base64.encodeBase64(searchItemProviderFormGroup.value.search),
      idProvider: this.base64.encodeBase64(idProvider)
    }
    await $.ajax({
      url: serviceAccessRoute + fornecedorItensRoute,
      data: {
        searchItemProvider: {
          tk: this.auth.getToken(),
          body: bodyConstruct
        }
      },
      type: 'GET',
      cache: false,
      success: function (response: any) {
        if (response)
          ItensFornecedorService.response = response;
      }
    });

    // Validando resposta do servidor
    if (this.responseTratament.validateResponse(ItensFornecedorService.response)) {
      let response = JSON.parse(ItensFornecedorService.response);
      this.subject.next(response);
    }
  }

  async deleteItemProvider(idProdutoFornecedor: number, idFornecedor: number) {
    let bodyConstruct = {
      idFornecedor: this.base64.encodeBase64(idFornecedor),
      idProdutoFornecedor: this.base64.encodeBase64(idProdutoFornecedor)
    }

    await $.ajax({
      url: serviceAccessRoute + fornecedorItensRoute,
      data: {
        deleteItemProvider: {
          tk: this.auth.getToken(),
          body: bodyConstruct
        }
      },
      type: 'POST',
      cache: false,
      success: function (response: any) {
        if (response)
          ItensFornecedorService.response = response;
      }
    });
    this.responseTratament.validateResponse(ItensFornecedorService.response);
  }

  async updateItemProvider(itemsUpdate: FormGroup, idProvider: number, idItemProvider: number) {
    let bodyConstruct = {
      idProvider: this.base64.encodeBase64(idProvider),
      idItemProvider: this.base64.encodeBase64(idItemProvider),
      descricao: this.base64.encodeBase64(itemsUpdate.value.formControlDescription),
      docNumero: this.base64.encodeBase64(itemsUpdate.value.formControlDocnumber),
      valorCompra: this.base64.encodeBase64(itemsUpdate.value.formControlBuyValue)
    }

    await $.ajax({
      url: serviceAccessRoute + fornecedorItensRoute,
      data: {
        updateItemProvider: {
          tk: this.auth.getToken(),
          body: bodyConstruct
        }
      },
      type: 'POST',
      cache: false,
      success: function (response: any) {
        if (response)
          ItensFornecedorService.response = response;
      }
    });
    this.responseTratament.validateResponse(ItensFornecedorService.response);
    this.getTableView(idProvider);
    this.router.navigate(['/estoque/fornecedor/itens/' + idProvider]);
  }
}
