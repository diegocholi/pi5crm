import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItensFornecedorComponent } from './itens-fornecedor.component';

describe('ItensFornecedorComponent', () => {
  let component: ItensFornecedorComponent;
  let fixture: ComponentFixture<ItensFornecedorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItensFornecedorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItensFornecedorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
