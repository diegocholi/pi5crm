import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TableItensProvider } from './itens-fornecedor-iterface';
import { ItensFornecedorService } from './itens-fornecedor.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ComponenteBase } from 'src/app/componente-base';

@Component({
  selector: 'appweb-itens-fornecedor',
  templateUrl: './itens-fornecedor.component.html',
  styleUrls: ['./itens-fornecedor.component.css']
})
export class ItensFornecedorComponent extends ComponenteBase implements OnInit {
  confirmDeleteMensage: boolean = false;
  idFornecedor: number;
  searchItemProviderFormGroup: FormGroup;

  tableViewItemsProvider: Array<TableItensProvider> = [{
    idProdutoFornecedor: null,
    idFornecedor: this.idFornecedor,
    descricao: '',
    numeroDocumento: '',
    valorCompra: null
  }]

  private idProdutoFornecedor;

  constructor(
    private activatedRoute: ActivatedRoute, private itemService: ItensFornecedorService,
    private formBuilder: FormBuilder) {
    super();
  }

  ngOnInit() {
    this.searchItemProviderFormGroup = this.formBuilder.group({
      search: ['']
    });

    this.activatedRoute.params.subscribe(param => {
      if (param) {
        this.idFornecedor = Number(param.idFornecedor);
      }
    });

    this.itemService.getTableItemProviderObservable().subscribe(subject => {
      this.tableViewItemsProvider = subject;
    });

    this.itemService.getTableView(this.idFornecedor);
  }


  searchItemProvider() {
    this.itemService.searchItemProvider(this.searchItemProviderFormGroup, this.idFornecedor);
  }

  openMesageConfirmDelete(idProdutoFornecedor: number) {
    this.idProdutoFornecedor = idProdutoFornecedor;
    this.confirmDeleteMensage = true;
  }

  confirmDelete(operation: boolean) {
    if (operation) {
      let arrayPosition = this.getPossitionArrayID(this.idProdutoFornecedor, this.tableViewItemsProvider);
      if (arrayPosition != null) {
        this.tableViewItemsProvider.splice(arrayPosition, 1);
        this.itemService.deleteItemProvider(this.idProdutoFornecedor, this.idFornecedor);
        this.confirmDeleteMensage = false;
      }
    } else {
      this.idProdutoFornecedor = null;
      this.confirmDeleteMensage = false;
    }
  }
}
