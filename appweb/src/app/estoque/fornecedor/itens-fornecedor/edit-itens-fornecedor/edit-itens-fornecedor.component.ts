import { Component, OnInit } from '@angular/core';
import { RequireFieldProvider, ButtonControl } from '../itens-fornecedor-iterface';
import { ActivatedRoute, Router } from '@angular/router';
import { ItensFornecedorService } from '../itens-fornecedor.service';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'appweb-edit-itens-fornecedor',
  templateUrl: './edit-itens-fornecedor.component.html',
  styleUrls: ['./edit-itens-fornecedor.component.css']
})
export class EditItensFornecedorComponent implements OnInit {

  descricao: string;
  idFornecedor: number;
  idProdutoFornecedor: number;
  numeroDocumento: string;
  valorCompra: number;

  require: RequireFieldProvider;
  buttonControlInterface: ButtonControl;
  buttonControl: boolean = true;

  editItemProviderFormGroup: FormGroup;

  constructor(private activatedRoutes: ActivatedRoute, private itemService: ItensFornecedorService,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.activatedRoutes.params.subscribe(params => {
      this.descricao = params.descricao;
      this.idFornecedor = params.idFornecedor;
      this.idProdutoFornecedor = params.idProdutoFornecedor;
      this.numeroDocumento = params.numeroDocumento;
      this.valorCompra = params.valorCompra;

      this.editItemProviderFormGroup = this.formBuilder.group({
        formControlDescription: [params.descricao],
        formControlDocnumber: [params.numeroDocumento],
        formControlBuyValue: [params.valorCompra]
      });
    });

    this.require = {
      description: false,
      docNumber: false,
      purchaseValue: false
    }

    this.buttonControlInterface = {
      description: true,
      docNumber: true,
      purchaseValue: true
    }
  }
  controlButtonInterface() {
    if (
      this.buttonControlInterface.description &&
      this.buttonControlInterface.docNumber &&
      this.buttonControlInterface.purchaseValue) {
      this.buttonControl = true;
    } else
      this.buttonControl = false;
  }

  updateItemProvider() {
    this.itemService.updateItemProvider(this.editItemProviderFormGroup, this.idFornecedor, this.idProdutoFornecedor);

  }

  formControlDescription() {
    if ($('#formControlDescription').val()) {
      $('#formControlDescription').css('borderColor', '#81DAF5');
      this.buttonControlInterface.description = true;
      this.require.description = false;
    } else {
      $('#formControlDescription').css('borderColor', 'red');
      this.buttonControlInterface.description = false;
      this.require.description = true;
    }
    this.controlButtonInterface();
  }

  formControlDocnumber() {
    if ($('#formControlDocnumber').val()) {
      $('#formControlDocnumber').css('borderColor', '#81DAF5');
      this.buttonControlInterface.docNumber = true;
      this.require.docNumber = false;
    } else {
      $('#formControlDocnumber').css('borderColor', 'red');
      this.buttonControlInterface.docNumber = false;
      this.require.docNumber = true;
    }
    this.controlButtonInterface();
  }

  formControlBuyValue() {
    if ($('#formControlBuyValue').val()) {
      $('#formControlBuyValue').css('borderColor', '#81DAF5');
      this.buttonControlInterface.purchaseValue = true;
      this.require.purchaseValue = false;
    } else {
      $('#formControlBuyValue').css('borderColor', 'red');
      this.buttonControlInterface.purchaseValue = false;
      this.require.purchaseValue = true;
    }
    this.controlButtonInterface();
  }

}
