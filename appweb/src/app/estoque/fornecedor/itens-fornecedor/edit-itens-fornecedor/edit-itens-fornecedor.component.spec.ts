import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditItensFornecedorComponent } from './edit-itens-fornecedor.component';

describe('EditItensFornecedorComponent', () => {
  let component: EditItensFornecedorComponent;
  let fixture: ComponentFixture<EditItensFornecedorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditItensFornecedorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditItensFornecedorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
