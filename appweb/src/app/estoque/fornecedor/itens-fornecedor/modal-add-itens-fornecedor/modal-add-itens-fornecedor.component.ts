import { Component, OnInit, Input } from '@angular/core';
import { RequireFieldProvider, ButtonControl, TableItensProvider } from '../itens-fornecedor-iterface';
import * as $ from 'jquery';
import { ItensFornecedorService } from '../itens-fornecedor.service';

@Component({
  selector: 'appweb-modal-add-itens-fornecedor',
  templateUrl: './modal-add-itens-fornecedor.component.html',
  styleUrls: ['./modal-add-itens-fornecedor.component.css']
})
export class ModalAddItensFornecedorComponent implements OnInit {
  @Input()
  idFornecedor: number;
  require: RequireFieldProvider;
  buttonControlInterface: ButtonControl;
  buttonControl: boolean = false;
  tableItensProvider: Array<TableItensProvider>;
  /**
    * @motalTitle Titulo do modal
    */
  @Input()
  motalTitle: string = 'Title Default Modal';

  /**
   * @buttonModalName : Edita o nome do botão do motal
   */
  @Input()
  buttonModalName: string = 'Title Default Button Modal';

  /**
  * @buttonClassCss : Configuração da classe css botstrap
  */
  @Input()
  buttonClassCss = 'btn btn-success';

  constructor(private itensFornecedorService: ItensFornecedorService) { }

  ngOnInit() {
    this.require = {
      description: false,
      docNumber: false,
      purchaseValue: false
    }

    this.buttonControlInterface = {
      description: false,
      docNumber: false,
      purchaseValue: false
    }

    this.tableItensProvider = [{
      idProdutoFornecedor: null,
      idFornecedor: this.idFornecedor,
      valorCompra: null,
      descricao: '',
      numeroDocumento: ''
    }]
  }

  controlButtonInterface() {
    if (
      this.buttonControlInterface.description &&
      this.buttonControlInterface.docNumber &&
      this.buttonControlInterface.purchaseValue) {
      this.buttonControl = true;
    } else
      this.buttonControl = false;
  }

  inputItemProvider() {
    if (this.buttonControl) {
      this.tableItensProvider = [
        {
          idProdutoFornecedor: null,
          idFornecedor: Number(this.idFornecedor),
          descricao: String($('#formControlDescription').val()),
          numeroDocumento: String($('#formControlDocnumber').val()),
          valorCompra: Number($('#formControlBuyValue').val())
        }
      ]
      // Inserindo itens
      this.itensFornecedorService.inputItemProvider(this.tableItensProvider);

      // Resetando view
      this.buttonControlInterface.description = false;
      this.require.description = false;
      this.buttonControlInterface.docNumber = false;
      this.require.docNumber = false;
      this.buttonControlInterface.purchaseValue = false;
      this.require.purchaseValue = false;
      $('#formControlBuyValue').css('borderColor', '#81DAF5');
      $('#formControlDescription').css('borderColor', '#81DAF5');
      $('#formControlDocnumber').css('borderColor', '#81DAF5');
      $('#formControlDescription').val('');
      $('#formControlDocnumber').val('');
      $('#formControlBuyValue').val('');
      this.controlButtonInterface();
    }
  }

  formControlDescription() {
    if ($('#formControlDescription').val()) {
      $('#formControlDescription').css('borderColor', '#81DAF5');
      this.buttonControlInterface.description = true;
      this.require.description = false;
    } else {
      $('#formControlDescription').css('borderColor', 'red');
      this.buttonControlInterface.description = false;
      this.require.description = true;
    }
    this.controlButtonInterface();
  }

  formControlDocnumber() {
    if ($('#formControlDocnumber').val()) {
      $('#formControlDocnumber').css('borderColor', '#81DAF5');
      this.buttonControlInterface.docNumber = true;
      this.require.docNumber = false;
    } else {
      $('#formControlDocnumber').css('borderColor', 'red');
      this.buttonControlInterface.docNumber = false;
      this.require.docNumber = true;
    }
    this.controlButtonInterface();
  }

  formControlBuyValue() {
    if ($('#formControlBuyValue').val()) {
      $('#formControlBuyValue').css('borderColor', '#81DAF5');
      this.buttonControlInterface.purchaseValue = true;
      this.require.purchaseValue = false;
    } else {
      $('#formControlBuyValue').css('borderColor', 'red');
      this.buttonControlInterface.purchaseValue = false;
      this.require.purchaseValue = true;
    }
    this.controlButtonInterface();
  }

}
