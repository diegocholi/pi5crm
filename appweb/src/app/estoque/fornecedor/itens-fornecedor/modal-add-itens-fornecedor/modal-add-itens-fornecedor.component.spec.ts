import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAddItensFornecedorComponent } from './modal-add-itens-fornecedor.component';

describe('ModalAddItensFornecedorComponent', () => {
  let component: ModalAddItensFornecedorComponent;
  let fixture: ComponentFixture<ModalAddItensFornecedorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAddItensFornecedorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAddItensFornecedorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
