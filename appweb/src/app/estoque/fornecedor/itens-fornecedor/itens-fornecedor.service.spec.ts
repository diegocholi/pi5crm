import { TestBed } from '@angular/core/testing';

import { ItensFornecedorService } from './itens-fornecedor.service';

describe('ItensFornecedorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ItensFornecedorService = TestBed.get(ItensFornecedorService);
    expect(service).toBeTruthy();
  });
});
