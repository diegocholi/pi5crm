import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAddFornecedorComponent } from './modal-add-fornecedor.component';

describe('ModalAddFornecedorComponent', () => {
  let component: ModalAddFornecedorComponent;
  let fixture: ComponentFixture<ModalAddFornecedorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAddFornecedorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAddFornecedorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
