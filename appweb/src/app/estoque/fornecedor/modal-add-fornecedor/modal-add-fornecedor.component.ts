import { Component, OnInit, Input } from '@angular/core';
import { FornecedorService } from '../fornecedor.service';
import { MensageControl, ButtonControl, Fornecedor } from '../fornecedor.modal.interface';

import * as $ from 'jquery';

@Component({
  selector: 'appweb-modal-add-fornecedor',
  templateUrl: './modal-add-fornecedor.component.html',
  styleUrls: ['./modal-add-fornecedor.component.css']
})

export class ModalAddFornecedorComponent implements OnInit {
  mensageLengh: boolean = false;
  mensageTax: boolean = false;
  constructor(private fornecedorService: FornecedorService) { }
  buttonDisabledControl: boolean = false;

  /**
   * @fornecedor Implementação de interface para envio de dados para o servidor
   */
  fornecedor: Fornecedor = {
    name: null,
    title: null,
    percent: null,
    phone: null
  }

  /**
   * @buttonControlAddInterface Interface que controla a mensagem de Require
   */
  mensageRequireControl: MensageControl = {
    name: false,
    title: false,
    percent: false,
    phone: false
  }

  /**
   * @buttonControlAddInterface Interface que controla o disable do botão
   */
  buttonControlAddInterface: ButtonControl = {
    name: false,
    title: false,
    percent: false,
    phone: false
  }

  /**
   * @motalTitle Titulo do modal
   */
  @Input()
  motalTitle: string = 'Title Default Modal';

  /**
   * @buttonModalName : Edita o nome do botão do motal
   */
  @Input()
  buttonModalName: string = 'Title Default Button Modal';

  /**
   * @buttonClassCss : Configuração da classe css botstrap
   */
  @Input()
  buttonClassCss = 'btn btn-success';

  ngOnInit() {
  }

  /**
   * @getStatusbuttonControlAddInterface Retorna o status da interface de controle do disable do botão
   */
  getStatusbuttonControlAddInterface(): boolean {
    if (
      this.buttonControlAddInterface.name &&
      this.buttonControlAddInterface.title &&
      this.buttonControlAddInterface.percent &&
      this.buttonControlAddInterface.phone
    ) {
      return true;
    }
    return false;
  }

  /**
   * @buttonDisableControl Método que controla o disabled do botão 
   */
  buttonControlAdd() {
    if (this.getStatusbuttonControlAddInterface()) {
      this.buttonDisabledControl = true;
    } else {
      this.buttonDisabledControl = false;
    }
  }

  /**
  * @formControlNameAdd Controle  de validação do campo nome do fornecedor
  */
  formControlNameAdd() {
    if ($('#formControlNameAdd').val()) {
      this.fornecedor.name = String($('#formControlNameAdd').val());

      // Validação se o nome do fornecedor tem mais de 3 caracteres
      if (this.fornecedor.name.length >= 3) {
        // Campo preenchido
        $('#formControlNameAdd').css('borderColor', '#81DAF5');
        this.mensageRequireControl.name = false; // Retira a mensagem de require do formulário
        this.mensageLengh = false; // Retira mensagem lengh
        /**
         * Define a propriedade de controle de botão como true, 
         * todas as propriedades devem ser true para ativar o botão
        */
        this.buttonControlAddInterface.name = true;
      } else {
        $('#formControlNameAdd').css('borderColor', 'red');
        this.mensageLengh = true; // Coloca mensagem lengh
        this.mensageRequireControl.name = false;

        /**
         * Define a propriedade de controle de botão como true, 
         * todas as propriedades devem ser true para ativar o botão
        */
        this.buttonControlAddInterface.name = false;
      }

    } else {
      $('#formControlNameAdd').css('borderColor', 'red');
      this.mensageRequireControl.name = true; // Coloca a mensagem de require do formulário
      this.mensageLengh = false; // Retira mensage lengh
      /**
       * Define a propriedade de controle de botão como true, 
       * todas as propriedades devem ser true para ativar o botão
      */
      this.buttonControlAddInterface.name = false;
    }
    this.buttonControlAdd();
  }

  /**
   * @formControlTitleAdd Controle  de validação do campo titulo do fornecedor
   */
  formControlTitleAdd() {
    if ($('#formControlTitleAdd').val()) {
      // Campo preenchido
      $('#formControlTitleAdd').css('borderColor', '#81DAF5');
      this.mensageRequireControl.title = false; // Retira a mensagem de require do formulário

      /**
       * Define a propriedade de controle de botão como true, 
       * todas as propriedades devem ser true para ativar o botão
      */
      this.buttonControlAddInterface.title = true;
      this.fornecedor.title = String($('#formControlTitleAdd').val());
    } else {
      $('#formControlTitleAdd').css('borderColor', 'red');
      this.mensageRequireControl.title = true; // Coloca a mensagem de require do formulário

      /**
       * Define a propriedade de controle de botão como true, 
       * todas as propriedades devem ser true para ativar o botão
      */
      this.buttonControlAddInterface.title = false;
    }
    this.buttonControlAdd();
  }

  /**
   * @formControlPercentAdd Controle  de validação do campo juros
   */
  formControlPercentAdd() {
    if ($('#formControlPercentAdd').val()) {
      this.fornecedor.percent = Number($('#formControlPercentAdd').val());

      // Verificação de porcentagem
      if (this.fornecedor.percent >= 0 && this.fornecedor.percent <= 100) {
        // Campo preenchido
        $('#formControlPercentAdd').css('borderColor', '#81DAF5');
        this.mensageRequireControl.percent = false; // Retira a mensagem de require do formulário
        this.mensageTax = false;

        /**
         * Define a propriedade de controle de botão como true, 
         * todas as propriedades devem ser true para ativar o botão
        */
        this.buttonControlAddInterface.percent = true;
      } else {
        $('#formControlPercentAdd').css('borderColor', 'red');
        this.mensageRequireControl.percent = false; // Coloca a mensagem de require do formulário
        this.mensageTax = true;
        /**
         * Define a propriedade de controle de botão como true, 
         * todas as propriedades devem ser true para ativar o botão
        */
        this.buttonControlAddInterface.percent = false;
      }
    } else {
      $('#formControlPercentAdd').css('borderColor', 'red');
      this.mensageRequireControl.percent = true; // Coloca a mensagem de require do formulário
      this.mensageTax = false;
      /**
       * Define a propriedade de controle de botão como true, 
       * todas as propriedades devem ser true para ativar o botão
      */
      this.buttonControlAddInterface.percent = false;
    }
    this.buttonControlAdd();
  }

  formControlPhoneAdd() {
    if ($('#formControlPhoneAdd').val()) {
      // Campo preenchido
      $('#formControlPhoneAdd').css('borderColor', '#81DAF5');
      this.mensageRequireControl.phone = false; // Retira a mensagem de require do formulário

      /**
       * Define a propriedade de controle de botão como true, 
       * todas as propriedades devem ser true para ativar o botão
      */
      this.buttonControlAddInterface.phone = true;
      this.fornecedor.phone = String($('#formControlPhoneAdd').val());
    } else {
      $('#formControlPhoneAdd').css('borderColor', 'red');
      this.mensageRequireControl.phone = true; // Coloca a mensagem de require do formulário

      /**
       * Define a propriedade de controle de botão como true, 
       * todas as propriedades devem ser true para ativar o botão
      */
      this.buttonControlAddInterface.phone = false;
    }
    this.buttonControlAdd();
  }

  inputRegisterProvider() {
    if (this.getStatusbuttonControlAddInterface()) {
      // Envia os dados para o servidor e mensagem de success
      this.resetModal();
      this.fornecedorService.insertFornecedor(this.fornecedor);
    } else {
      // Mensagem de erro
    }
  }

  resetModal() {
    $('#formControlPercentAdd').val('');
    $('#formControlTitleAdd').val('');
    $('#formControlNameAdd').val('');
    $('#formControlPhoneAdd').val('');
    this.buttonControlAddInterface.name = false;
    this.buttonControlAddInterface.title = false;
    this.buttonControlAddInterface.percent = false;
    this.buttonControlAddInterface.phone = false;
    this.buttonDisabledControl = false;
  }
}
