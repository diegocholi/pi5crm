import { Component, OnInit } from '@angular/core';
import { FornecedorService } from './fornecedor.service';
import { TableViewFornecedor, Fornecedor } from './fornecedor.modal.interface';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ComponenteBase } from 'src/app/componente-base';

@Component({
  selector: 'appweb-fornecedor',
  templateUrl: './fornecedor.component.html',
  styleUrls: ['./fornecedor.component.css']
})

export class FornecedorComponent extends ComponenteBase implements OnInit {
  inscription: Subscription;
  tableViewFornecedor: Array<TableViewFornecedor>;
  searchProviderFormGroup: FormGroup;
  form: FormGroup;
  confirmDelete: boolean = false;
  idProvider: number = 0;
  fornecedor: Fornecedor = {
    name: '',
    percent: null,
    phone: '',
    title: ''
  };

  dateRegister: string; // Propriedade declarada para recebimento local dos valores de edição

  constructor(private service: FornecedorService, private formBuilder: FormBuilder) {
    super();
    this.searchProviderFormGroup = this.formBuilder.group({
      search: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.service.getTableProviderView();
    // Lógica que implementa o observable para exibir mensagens na tela caso haja algum erro
    this.inscription = this.service.getTableObservable().subscribe(tableViewProvider => {
      if (tableViewProvider) {
        if (tableViewProvider['response'])
          this.tableViewFornecedor = tableViewProvider['response'];

        if (tableViewProvider['tableViewProvider'])
          this.tableViewFornecedor = tableViewProvider['tableViewProvider'];
      } else
        this.service.clearTableProvider();

    });
  }

  onDeleteProvider(idDelete: number) {
    this.confirmDelete = true;
    this.idProvider = idDelete;
  }

  deleteConfirmMensage(action: boolean) {
    if (action) {
      let arrayPosition = this.getPossitionArrayID(this.idProvider, this.tableViewFornecedor);
      this.tableViewFornecedor.splice(arrayPosition, 1);
      this.service.deleteProvider(this.idProvider);
      this.confirmDelete = false;
    } else {
      this.idProvider = null;
      this.confirmDelete = false;
    }
  }

  updateProvider(id: number, name: string, phone: string, title: string, percent: number) {
    this.fornecedor = {
      name: name,
      percent: percent,
      phone: phone,
      title: title
    };
  }

  /**
   * Método do botão bustar por fornecedor
   */
  searchProvide() {
    this.service.searchProvider(this.searchProviderFormGroup);
  }

  ngOnDestroy() {
    this.inscription.unsubscribe();
  }
}
