export interface MensageControl {
    name: boolean
    title: boolean
    percent: boolean,
    phone: boolean
}

export interface ButtonControl {
    name: boolean
    title: boolean
    percent: boolean,
    phone: boolean
}

export interface Fornecedor {
    name: string
    title: string
    percent: number,
    phone: string
}

export interface TableViewFornecedor {
    idFornecedor: number
    dataRegistro: string
    nome: string
    taxaJuros: string
    tituloPortador: string
    contato: string
}