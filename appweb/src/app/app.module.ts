import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { RouterModule, PreloadAllModules } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ROUTES } from './app.routes';
import { HomeComponent } from './home/home.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { LoginComponent } from './login/login.component';
import { AuthService } from './login/authService/auth.service';
import { AuthGuardService } from './guard/auth.guard.service';
import { MensageService } from './service/mensage.service';
import { CaixaModule } from './caixa/caixa.module';
import { EstoqueModule } from './estoque/estoque.module';
import { Base64Service } from './service/base64.service';
import { ContasComponent } from './contas/contas.component';
import { ContasModalComponent } from './contas/contas-modal/contas-modal.component';
import { FluxoBancarioComponent } from './fluxo-bancario/fluxo-bancario.component';
import { EditContaComponent } from './contas/edit-conta/edit-conta.component';
import { FluxoModalComponent } from './fluxo-bancario/fluxo-modal/fluxo-modal.component';
import { EditFluxoComponent } from './fluxo-bancario/edit-fluxo/edit-fluxo.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    SidebarComponent,
    LoginComponent,
    ContasComponent,
    ContasModalComponent,
    FluxoBancarioComponent,
    EditContaComponent,
    FluxoModalComponent,
    EditFluxoComponent
  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    CaixaModule,
    EstoqueModule,
    RouterModule.forRoot(ROUTES, { preloadingStrategy: PreloadAllModules })
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    AuthService,
    AuthGuardService,
    MensageService,
    Base64Service
  ],
  exports: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
