import { Component, OnInit, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from './authService/auth.service';
import { MensageService } from '../service/mensage.service';

interface MensageInputControl {
  userInputMensageControl: boolean;
  passwordInputMensageControl: boolean;
}

@Component({
  selector: 'appweb-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  formLogin: FormGroup;
  checkbox: boolean = false;
  mensageInputControl: MensageInputControl = {
    userInputMensageControl: false,
    passwordInputMensageControl: false
  }

  constructor(private authService: AuthService,
    private formBuilder: FormBuilder,
    private messageService: MensageService) {
  }

  ngOnInit() {
    this.formLogin = this.formBuilder.group({
      user: ["", Validators.required],
      password: ["", Validators.required],
      checkbox: [false]
    });
  }

  login() {
    this.authService.auth(this.formLogin); // Autenticação do usuário
    this.authService.openMenuEmitter.subscribe((statusUser: boolean) => {
      // Lógica de feedbacks do usuário
      if (
        !statusUser &&  // Verificando se o usuário existe no banco para exibição da mensagem
        this.formLogin.valid && // Verificação se todos os campos estão preenchidos através do  Validators.required
        !this.messageService.mensageControl // variavel de controle de mensagem para evitar multiplicidade de mensagens
      ) {
        // Lógica de validação login de usuário
        this.messageService.sendMessage('Erro de usuário ou senha');
        this.messageService.mensageControl = true;

        setTimeout(() => {
          this.messageService.mensageControl = false;
          this.messageService.clearMessages();
        }, 16000);

      }
      if (this.formLogin.controls.user.status === 'INVALID') {
        console.log(this.formLogin.controls.user.status);
        this.validateUserForm();
      }
      if (this.formLogin.controls.password.status === 'INVALID') {
        console.log(this.formLogin.controls.password.status);
        this.validatePasswordForm();
      }
    });
  }

  opacyLoginControl() {
    document.getElementById('login').style.opacity = '1';
  }

  // Validação Require User
  validateUserForm() {
    if (this.formLogin.controls.user.status === 'INVALID') {
      document.getElementById("user").style.borderColor = 'red';
      this.mensageInputControl.userInputMensageControl = true;
    } else {
      document.getElementById("user").style.borderColor = '#81DAF5';
      this.mensageInputControl.userInputMensageControl = false;
    }
  }

  // Validação Require Password
  validatePasswordForm() {
    if (this.formLogin.controls.password.status === 'INVALID') {
      document.getElementById("password").style.borderColor = 'red';
      this.mensageInputControl.passwordInputMensageControl = true;
    } else {
      document.getElementById("password").style.borderColor = '#81DAF5';
      this.mensageInputControl.passwordInputMensageControl = false;
    }
  }
}
