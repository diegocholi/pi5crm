import { Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { serviceAccessRoute, auth } from '../../configuration/serviceConfig';
import { Base64Service } from '../../service/base64.service';

import * as $ from 'jquery';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  openMenuEmitter = new EventEmitter<boolean>();
  private static autorizationUserTK: string;
  constructor(private router: Router, private base64Service: Base64Service) {
  }

  async auth(formLogin: FormGroup) {
    localStorage.setItem('token', 'false');

    var dateUser = {
      user: this.base64Service.encodeBase64(formLogin.value.user),
      password: this.base64Service.encodeBase64(formLogin.value.password)
    }

    await $.ajax({
      url: serviceAccessRoute + auth,
      data: {
        dateUser: dateUser
      },
      cache: false,
      type: 'GET',
      success: function (response) {
        response = JSON.parse(response);
        if (response != '401: Unauthorized') {
          // Autorizado
          AuthService.autorizationUserTK = response;
        } else {
          // Não autorizado
          AuthService.autorizationUserTK = null;
          console.log(response);
        }
      }
    });

    if (AuthService.autorizationUserTK) {
      if (formLogin.value.checkbox === true)
        localStorage.setItem('token', AuthService.autorizationUserTK);

      this.openMenuEmitter.emit(true);
      this.router.navigate(['/']);
    }

    else {
      this.openMenuEmitter.emit(false);
      localStorage.setItem('token', 'false');
    }
  }

  signOut() {
    AuthService.autorizationUserTK = null;
    localStorage.setItem('token', 'false');
    this.router.navigate(['/login']);
    location.reload();
  }

  getStatusUser() {
    if (localStorage.getItem('token') != 'false' || AuthService.autorizationUserTK != null)
      return true;
    else
      return false;
  }

  getToken() {
    if (AuthService.autorizationUserTK != undefined)
      return AuthService.autorizationUserTK;
    else {
      if (localStorage.getItem('token') != 'false') {
        return localStorage.getItem('token')
      }
    }
    return false;
  }

}
