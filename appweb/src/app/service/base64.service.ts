import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Base64Service {

  constructor() {

  }

  encodeBase64(value: any) {
    // Convertendo para Base64
    return btoa(value);
  }

  decodeBase64(value: any) {
    // Voltando para string
    return atob(value);
  }
}
