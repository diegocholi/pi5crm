import { TestBed } from '@angular/core/testing';

import { JqueryTratamentResponseService } from './jquery-tratament-response.service';

describe('JqueryTratamentResponseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JqueryTratamentResponseService = TestBed.get(JqueryTratamentResponseService);
    expect(service).toBeTruthy();
  });
});
