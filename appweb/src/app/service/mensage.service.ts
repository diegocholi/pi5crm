import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MensageService {
  mensageControl: boolean = false;
  private subject = new Subject<any>();

  constructor() { }
  sendMessage(message: string) {
    this.subject.next({ text: message });
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }

  clearMessages() {
    this.subject.next();
  }
}
