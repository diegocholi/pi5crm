import { Injectable } from '@angular/core';
import { AuthService } from '../login/authService/auth.service';

@Injectable({
  providedIn: 'root'
})
export class JqueryTratamentResponseService {

  constructor(private auth: AuthService) { }

  /**
   * @validateResponse valida a resposta do ajax 
   * @response resposta da requisição ajax
  */
  public validateResponse(response: any) {
    if (response) {
      switch (response) {
        case 'SUCCESS: 200 OK':
          console.log(response);
          break;
        case 'SUCCESS: 201 Created':
          console.log(response);
          break;
        case 'ERRO: 401 Unauthorized':
          console.log(response);
          this.redirect401Erro()
          break;
        case 'ERRO: 403 Forbidden':
          console.log(response);
          break;
        case 'ERRO: 404 Not Found':
          console.log(response);
          break;
        case 'ERRO: 422 Unprocessable Entity':
          console.log(response);
          break;
        default:
          return true;
      }
    }
  }

  /**
   *  @redirect401Erro Verificação se usuário tem um token válido com base na resposta do servidor
  */
  redirect401Erro() {
    this.auth.signOut();
  }

}
