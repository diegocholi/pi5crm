import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MensageService } from '../../service/mensage.service'
import * as $ from 'jquery';
import { CaixaServiceService } from '../caixa-service.service';
import { EstoqueService } from 'src/app/estoque/estoque.service';
import { EntradaInterfaceSelectView, InterfaceContaSelectView } from './entrada-interface';
import { ContasService } from 'src/app/contas/contas.service';

@Component({
  selector: 'appweb-entrada',
  templateUrl: './entrada.component.html',
  styleUrls: ['./entrada.component.css', '../caixa.component.css']
})

export class EntradaComponent implements OnInit {
  selectItensStorageControl: boolean = false; // Controle de formulário: verifica se irá receber itens do banco de dados ou a descrição será feita pelo usuário
  formCaixaEntrada: FormGroup;
  buttonDisabledControl: boolean = true;
  selectViewEstoque: EntradaInterfaceSelectView[] = [];
  selectViewContas: InterfaceContaSelectView[] = [];
  controlMensageRequire: any = {
    operation: false,
    quantity: false,
    value: false,
    storageItens: false
  }

  constructor(private formBuilder: FormBuilder, private serviceStorage: EstoqueService,
    private serviceCaixa: CaixaServiceService, private serviceContas: ContasService) { }

  ngOnInit() {
    this.serviceContas.getContasForCaixa("entrada");
    this.serviceContas.getSubject().subscribe(params => {
      this.selectViewContas = params;
      this.selectViewEstoque = [];

    });

    this.serviceStorage.getSubject().subscribe(params => {
      this.selectViewEstoque = params;
      this.selectViewContas = [];

    });
    // Groupo do formulário de caixa
    this.formCaixaEntrada = this.formBuilder.group({
      storageItens: [false],
      descriptionOperation: ['', Validators.required],
      itensQuantity: ['', Validators.required],
      selectItemStorage: ['', Validators.required],
      selectItemConta: ['', Validators.required]
    });
  }

  insertEntradaCaixa() {
    if (
      this.formCaixaEntrada.value.selectItemConta
    ) {
      this.serviceCaixa.insertEntradaCaixa(this.formCaixaEntrada);
    } else if (
      this.formCaixaEntrada.value.selectItemStorage &&
      this.formCaixaEntrada.value.itensQuantity
    ) {
      this.serviceCaixa.insertEntradaCaixa(this.formCaixaEntrada);
    } else {
      alert('Preencha todos os campos !');
    }
  }

  storageItensControl() {
    if (this.formCaixaEntrada.value.storageItens) {
      this.selectItensStorageControl = true;
      this.formCaixaEntrada.value.descriptionOperation = false;
      this.serviceStorage.getItemStorageForCaixa();
    } else {
      this.formCaixaEntrada.value.selectItemStorage = false;
      this.selectItensStorageControl = false;
      this.serviceContas.getContasForCaixa("entrada");
    }
  }

  validatorDescriptionOperation() {
    if (this.formCaixaEntrada.value.descriptionOperation) {
      this.buttonDisabledControl = true;
      this.controlMensageRequire.operation = false;
      $('#descriptionOperation').css('borderColor', '#81DAF5');
    } else {
      this.buttonDisabledControl = false;
      this.controlMensageRequire.operation = true;
      $('#descriptionOperation').css('borderColor', 'red');
    }
  }

  validatorQuantityItems() {
    if (this.formCaixaEntrada.value.itensQuantity) {
      this.buttonDisabledControl = true;
      this.controlMensageRequire.quantity = false;
      $('#itensQuantity').css('borderColor', '#81DAF5');
    } else {
      this.buttonDisabledControl = false;
      this.controlMensageRequire.quantity = true;
      $('#itensQuantity').css('borderColor', 'red');
    }
  }

  validatorValueStorage() {
    if (this.formCaixaEntrada.value.valueInputStorage) {
      this.buttonDisabledControl = true;
      this.controlMensageRequire.value = false;
      $('#valueInputStorage').css('borderColor', '#81DAF5');
    } else {
      this.buttonDisabledControl = false;
      this.controlMensageRequire.value = true;
      $('#valueInputStorage').css('borderColor', 'red');
    }
  }
  validatorSelectItemStorage() {
    if (this.formCaixaEntrada.value.selectItemStorage) {
      // storageItens
      this.buttonDisabledControl = true;
      this.controlMensageRequire.storageItens = false;
      $('#selectItemStorage').css('borderColor', '#81DAF5');
    } else {
      this.buttonDisabledControl = false;
      this.controlMensageRequire.storageItens = true;
      $('#selectItemStorage').css('borderColor', 'red');
    }
  }
  validatorSelectItemConta() {
    if (this.formCaixaEntrada.value.selectItemConta) {
      // storageItens
      this.buttonDisabledControl = true;
      this.controlMensageRequire.storageItens = false;
      $('#selectItemConta').css('borderColor', '#81DAF5');
    } else {
      this.buttonDisabledControl = false;
      this.controlMensageRequire.storageItens = true;
      $('#selectItemConta').css('borderColor', 'red');
    }
  }
}
