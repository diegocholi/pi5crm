export interface EntradaInterfaceSelectView {
    descricao: string
    idProdutoFornecedor: string
    quantidade: string
}

export interface InterfaceContaSelectView {
    idPagamentos: string
    descricao: string
    valor: string
}

