import { Routes } from "@angular/router";
import { EntradaComponent } from './entrada/entrada.component';
import { SaidaComponent } from './saida/saida.component';
import { AuthGuardService } from '../guard/auth.guard.service';
import { CaixaComponent } from './caixa.component';

export const caixaRoutes: Routes = [
    { path: 'caixa', component: CaixaComponent, canActivate: [AuthGuardService] },
    { path: 'caixa/entrada', component: EntradaComponent, canActivate: [AuthGuardService] },
    { path: 'caixa/saida', component: SaidaComponent, canActivate: [AuthGuardService] }
]

