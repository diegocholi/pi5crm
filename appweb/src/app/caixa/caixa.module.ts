import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { caixaRoutes } from './caixa.router';
import { EntradaComponent } from './entrada/entrada.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CaixaComponent } from './caixa.component';
import { SaidaComponent } from './saida/saida.component';



@NgModule({
  declarations: [
    EntradaComponent,
    CaixaComponent,
    SaidaComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(caixaRoutes)
  ]
})
export class CaixaModule { }
