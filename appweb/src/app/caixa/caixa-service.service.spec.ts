import { TestBed } from '@angular/core/testing';

import { CaixaServiceService } from './caixa-service.service';

describe('CaixaServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CaixaServiceService = TestBed.get(CaixaServiceService);
    expect(service).toBeTruthy();
  });
});
