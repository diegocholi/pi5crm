import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CaixaServiceService } from '../caixa-service.service';
import { ContasService } from 'src/app/contas/contas.service';
import { InterfaceSaidaSelectView } from './interface-saida';

@Component({
  selector: 'appweb-saida',
  templateUrl: './saida.component.html',
  styleUrls: ['./saida.component.css', '../caixa.component.css'],
})
export class SaidaComponent implements OnInit {
  formCaixaSaida: FormGroup;
  selectView: InterfaceSaidaSelectView[] = [];
  selectAccountControl: boolean = false; // Controle se o usuário irá digitar uma descrição ou irá puxar contas fixa do BD
  constructor(private formBuilder: FormBuilder, private service: CaixaServiceService, private serviceContas: ContasService) { }

  ngOnInit() {
    this.serviceContas.getContasForCaixa("saida");
    this.serviceContas.getSubject().subscribe(params => {
      this.selectView = params;
    });
    // Groupo do formulário de caixa
    this.formCaixaSaida = this.formBuilder.group({
      fixedAccount: ['', Validators.required],
      itemSelecionado: ['', Validators.required],
      valor: ['', Validators.required],
      descricaoConta: ['', Validators.required]
    });
  }

  fixedAccount() {
    if (this.formCaixaSaida.value.fixedAccount) {
      this.selectAccountControl = true;
      this.serviceContas.getContasForCaixa("saida");
    } else {
      this.selectAccountControl = false;
    }
  }

  insertSaidaCaixa() {
    if (this.formCaixaSaida.value.itemSelecionado) {
      this.service.insertSaidaCaixa(this.formCaixaSaida);
    } else if (!this.formCaixaSaida.value.fixedAccount && this.formCaixaSaida.value.valor && this.formCaixaSaida.value.descricaoConta) {
      this.service.insertSaidaCaixa(this.formCaixaSaida);
    } else {
      alert("Preencha todos os campos !");
    }
  }
}
