export interface InterfaceSaidaSelectView {
    idPagamentos: string
    descricao: string
    valor: string
}
