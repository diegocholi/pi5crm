export interface InterfaceCaixaTableView {
    tabela: string
    idCaixa: string
    descricao: string
    operacao: string
    valor: string
    quantidade: string
    idProduto: string
}
