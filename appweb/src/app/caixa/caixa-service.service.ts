import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import * as $ from 'jquery';
import { serviceAccessRoute, caixa } from '../configuration/serviceConfig';
import { AuthService } from '../login/authService/auth.service';
import { JqueryTratamentResponseService } from '../service/jquery-tratament-response.service';
import { Router } from '@angular/router';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CaixaServiceService {
  private subject = new Subject<any>();
  private static response: any;
  constructor(private auth: AuthService, private jqueryTratament: JqueryTratamentResponseService, private router: Router) { }

  getSubject(): Observable<any> {
    return this.subject.asObservable();
  }

  async getTableView() {
    await $.ajax({
      url: serviceAccessRoute + caixa,
      data: {
        getTableView: {
          tk: this.auth.getToken(),
          body: null
        }
      },
      cache: false,
      type: 'GET',
      success: function (response) {
        CaixaServiceService.response = response;
      }
    });

    if (this.jqueryTratament.validateResponse(CaixaServiceService.response)) {
      CaixaServiceService.response = JSON.parse(CaixaServiceService.response);
      this.subject.next(CaixaServiceService.response);
    }
    CaixaServiceService.response = undefined;
  }

  async insertEntradaCaixa(formEntradaCaixa: FormGroup) {
    // SET CONTA A RECEBER CAIXA
    if (formEntradaCaixa.value.selectItemConta) {
      // SET ITEMS EM ESTOQUE CAIXA
      await $.ajax({
        url: serviceAccessRoute + caixa,
        data: {
          contaInsertEntradaCaixa: {
            tk: this.auth.getToken(),
            body: formEntradaCaixa.value.selectItemConta
          }
        },
        cache: false,
        type: 'POST',
        success: function (response) {
          CaixaServiceService.response = response;
        }
      });
    } else {
      let dados = {
        storageItens: formEntradaCaixa.value.storageItens,
        descriptionOperation: formEntradaCaixa.value.descriptionOperation,
        itensQuantity: formEntradaCaixa.value.itensQuantity,
        valueInputStorage: formEntradaCaixa.value.valueInputStorage,
        selectItemStorage: formEntradaCaixa.value.selectItemStorage ? formEntradaCaixa.value.selectItemStorage : null
      }

      await $.ajax({
        url: serviceAccessRoute + caixa,
        data: {
          insertEntradaCaixa: {
            tk: this.auth.getToken(),
            body: dados
          }
        },
        cache: false,
        type: 'POST',
        success: function (response) {
          CaixaServiceService.response = response;
        }
      });
    }
    this.jqueryTratament.validateResponse(CaixaServiceService.response);
    CaixaServiceService.response = undefined;
    this.router.navigate(['/caixa']);
  }

  async deleteItemCaixa(id: number) {
    await $.ajax({
      url: serviceAccessRoute + caixa,
      data: {
        deleteItemCaixa: {
          tk: this.auth.getToken(),
          body: id
        }
      },
      cache: false,
      type: 'POST',
      success: function (response) {
        CaixaServiceService.response = response;
      }
    });
    this.jqueryTratament.validateResponse(CaixaServiceService.response)
    CaixaServiceService.response = undefined;
  }

  async insertSaidaCaixa(formCaixaSaida: FormGroup) {
    let dados = {
      descricao: formCaixaSaida.value.descricaoConta,
      fixAcontCheck: formCaixaSaida.value.fixedAccount,
      idConta: formCaixaSaida.value.itemSelecionado,
      valor: formCaixaSaida.value.valor
    }
    await $.ajax({
      url: serviceAccessRoute + caixa,
      data: {
        insertSaidaCaixa: {
          tk: this.auth.getToken(),
          body: dados
        }
      },
      cache: false,
      type: 'POST',
      success: function (response) {
        CaixaServiceService.response = response;
      }
    });
    this.jqueryTratament.validateResponse(CaixaServiceService.response);
    CaixaServiceService.response = undefined;
    this.router.navigate(['/caixa']);

  }

  async getValuesDashboard() {
    await $.ajax({
      url: serviceAccessRoute + caixa,
      data: {
        getValuesDashboard: {
          tk: this.auth.getToken(),
          body: null
        }
      },
      cache: false,
      type: 'GET',
      success: function (response) {
        CaixaServiceService.response = response;
      }
    });

    if (this.jqueryTratament.validateResponse(CaixaServiceService.response)) {
      CaixaServiceService.response = JSON.parse(CaixaServiceService.response);
      this.subject.next(CaixaServiceService.response);
    }
    CaixaServiceService.response = undefined;
  }



}
