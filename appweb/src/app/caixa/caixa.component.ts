import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { InterfaceCaixaTableView } from './interface-caixa-table';
import { CaixaServiceService } from './caixa-service.service';
import { ComponenteBase } from '../componente-base';
import { EstoqueService } from '../estoque/estoque.service';

@Component({
  selector: 'appweb-caixa',
  templateUrl: './caixa.component.html',
  styleUrls: ['./caixa.component.css']
})
export class CaixaComponent extends ComponenteBase implements OnInit {
  tableView: InterfaceCaixaTableView[] = [];
  confirmDelete: boolean = false;
  formOperation: FormGroup;
  idCaixa: number;
  quantidade: number;
  idProduto: number;
  controlFormCaixa: number = 0; // Variavel que controla o tipo de formulário que será exibido

  constructor(private formBuilder: FormBuilder,
    private router: Router, private service: CaixaServiceService, private serviceEstoque: EstoqueService) {
    super();
  }

  ngOnInit() {
    this.service.getTableView();
    this.service.getSubject().subscribe(params => {
      this.tableView = params;
    });
    // Grupo de seleção para tipo de operação
    this.formOperation = this.formBuilder.group({
      operation: [''],
    });
  }

  deleteContaItem(idCaixa: number, quantidade: string, idProduto: string) {
    this.quantidade = Number(quantidade);
    this.idProduto = Number(idProduto);
    this.idCaixa = idCaixa;
    this.confirmDelete = true;
  }

  responseConfirmDelete(action: boolean) {
    let arrayPosition = this.getPossitionArrayID(Number(this.idCaixa), this.tableView);
    this.tableView.splice(arrayPosition, 1);
    if (action) {
      if (this.quantidade != 0 && this.quantidade) {
        this.service.deleteItemCaixa(this.idCaixa);
        this.serviceEstoque.updateStorageItemForCaixa(this.idProduto, this.quantidade);
        this.confirmDelete = false;
      } else {
        this.service.deleteItemCaixa(this.idCaixa);
        this.confirmDelete = false;
      }
    } else {
      this.confirmDelete = false;
    }
  }

  operationControl() {
    if (this.formOperation.value.operation === '1') { // 1: Entrada
      this.router.navigate(['/caixa/entrada']);
    } if (this.formOperation.value.operation === '2') { // 2: Saida
      this.router.navigate(['/caixa/saida']);
    }
  }
}
