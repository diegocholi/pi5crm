import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { CaixaServiceService } from '../caixa/caixa-service.service';
import { ChartBarras } from './charts.interface';
import { EstoqueService } from '../estoque/estoque.service';

interface ItemsEstoqueDash {
  descricao: string
  quantidade_caixa: string
  quantidade_estoque: string
  vendas: string
}


@Component({
  selector: 'appweb-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})


export class HomeComponent implements OnInit {

  constructor(private caixaService: CaixaServiceService, private estoqueService: EstoqueService) { }
  private interfaceBarras: ChartBarras[];
  labels: any[] = [];
  data: any[] = [];

  itemsEmFalta: Array<ItemsEstoqueDash> = [];
  ranking: Array<ItemsEstoqueDash> = [];
  rankingItensMais: Array<ItemsEstoqueDash> = [];
  valorAnual: number = 0;

  ngOnInit() {
    this.estoqueService.getIDashboardEstoque();
    this.estoqueService.getSubject().subscribe(params => {

      params.forEach(element => {
        if (element['quantidade_estoque'] <= 10) {
          this.itemsEmFalta.push(element);
          alert("Atenção existem itens em falta no estoque!");
        }

      });

      // Retornando ranking de vendas
      this.ranking = params.sort((a, b) => {
        return (b.vendas - a.vendas)
      });

      // Retornando ranking de vendas
      this.rankingItensMais = params.sort((a, b) => {
        return (b.quantidade_caixa - a.quantidade_caixa)
      });
      console.log(params);
    });

    this.caixaService.getValuesDashboard();
    this.caixaService.getSubject().subscribe(params => {
      if (params) {
        this.interfaceBarras = params;
        this.interfaceBarras.forEach(element => {
          this.labels.push(element.data);
          this.data.push(element.valor);
          this.valorAnual += Number(element.valor);
        });
        if (this.valorAnual > 81000) {
          alert("Atenção sua receita anual está acima do esperado para MEI");
        }
        var ctx1 = document.getElementById('myChart-1');
        new Chart(ctx1, {
          type: 'bar',
          data: {
            labels: this.labels,
            datasets: [{
              label: 'Receita Bruta',
              data: this.data,
              backgroundColor: [
                'rgba(255, 99, 132, 0.7)',
                'rgba(54, 162, 235, 0.7)',
                'rgba(255, 206, 86, 0.7)',
                'rgba(75, 192, 192, 0.7)',
                'rgba(153, 102, 255, 0.7)',
                'rgba(255, 159, 64, 0.7)',
                'rgba(255, 99, 132, 0.7)',
                'rgba(54, 162, 235, 0.7)',
                'rgba(255, 206, 86, 0.7)',
                'rgba(75, 192, 192, 0.7)',
                'rgba(153, 102, 255, 0.7)',
                'rgba(255, 159, 64, 0.7)'
              ],
              borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
              ],
              borderWidth: 1
            }]
          },
          options: {
            legend: {
              display: false
            },
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true
                }
              }]
            }
          }
        });

        var ctx2 = document.getElementById('myChart-2');

        new Chart(ctx2, {
          type: 'line',
          backgroundColor: 'rgba(153, 102, 255, 1)',
          data: {
            datasets: [{
              label: 'Crescimento/Mes',
              data: this.data,
              backgroundColor: "rgba(0, 0, 0, 0.1)",
              borderColor: "#82FA58",
            }],
            labels: this.labels
          },
          options: {
            legend: {
              display: false
            }
          }
        });
      }

    });
  }

}
