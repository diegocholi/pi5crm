export interface ChartBarras {
    data: string
    descricao: string
    idCaixa: string
    idProduto: string
    operacao: string
    quantidade: string
    tabela: string
    valor: string
}
