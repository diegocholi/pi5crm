export class ComponenteBase {
    /**
    * Método que retorna o id do array com base no id do objeto
    */
    getPossitionArrayID(id: number, tableView) {
        let arrayPosition = 0;
        let i: number = 0;
        tableView.forEach(element => {
            if (id === Number(element.idPagamentos)) {
                arrayPosition = i;
            }
            i++;
        });

        return arrayPosition;
    }
}
