import { Component, OnInit } from '@angular/core';
import { ContasService } from './contas.service';
import { InterfaceContaTableView } from './interface-contas';
import { ComponenteBase } from '../componente-base';
import * as $ from 'jquery';

@Component({
  selector: 'appweb-contas',
  templateUrl: './contas.component.html',
  styleUrls: ['./contas.component.css']
})
export class ContasComponent extends ComponenteBase implements OnInit {
  tableView: Array<InterfaceContaTableView> = [];
  confirmDeleteMensage: boolean = false;
  mensagemConfirmDelte: boolean = false;
  idItemDelete: Number = 0;

  constructor(private service: ContasService) {
    super();
  }

  ngOnInit() {
    this.service.getTableView();
    this.service.getSubject().subscribe(items => {
      this.tableView = items;
    });
  }

  deleteContaItem(id) {
    this.idItemDelete = id;
    this.mensagemConfirmDelte = true;
  }

  responseDeleteMensage(action: boolean) {
    if (action) {
      let arrayPosition = this.getPossitionArrayID(Number(this.idItemDelete), this.tableView);
      this.tableView.splice(arrayPosition, 1);
      this.service.deleteContaItem(Number(this.idItemDelete));
      this.mensagemConfirmDelte = false;
    } else {
      this.mensagemConfirmDelte = false;
    }
  }

  buscaConta() {
    this.service.buscaConta($('#search').val());
  }
}
