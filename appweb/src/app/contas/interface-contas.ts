export interface InterfaceContaTableView {
    idPagamentos: string
    descricao: string
    descMov: string
    dataVencimento: string
    valor: string
    status: string
}
