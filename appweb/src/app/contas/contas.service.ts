import { Injectable } from '@angular/core';
import { AuthService } from '../login/authService/auth.service';
import * as $ from 'jquery';
import { serviceAccessRoute, contas } from '../configuration/serviceConfig';
import { Subject, Observable } from 'rxjs';
import { JqueryTratamentResponseService } from '../service/jquery-tratament-response.service';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ContasService {
  private subject = new Subject<any>();
  private static response: any;

  constructor(private auth: AuthService, private jqueryTratament: JqueryTratamentResponseService) { }

  getSubject(): Observable<any> {
    return this.subject.asObservable();
  }

  async getTableView() {
    await $.ajax({
      url: serviceAccessRoute + contas,
      data: {
        getTableView: {
          tk: this.auth.getToken(),
          body: null
        }
      },
      cache: false,
      type: 'GET',
      success: function (response) {
        ContasService.response = response;
      }
    });

    if (this.jqueryTratament.validateResponse(ContasService.response)) {
      ContasService.response = JSON.parse(ContasService.response);
      this.subject.next(ContasService.response);
    }
    ContasService.response = undefined;
  }

  async deleteContaItem(id: number) {
    await $.ajax({
      url: serviceAccessRoute + contas,
      data: {
        deleteContaItem: {
          tk: this.auth.getToken(),
          body: id
        }
      },
      cache: false,
      type: 'POST',
      success: function (response) {
        ContasService.response = response;
      }
    });
    this.jqueryTratament.validateResponse(ContasService.response)
    ContasService.response = undefined;

  }

  async inserirConta(
    formControlValor: number,
    formControlDescricaoAdd: string,
    formControlData: string,
    formControlOperacao: string
  ) {

    let dados = {
      valor: formControlValor,
      descricao: formControlDescricaoAdd,
      data: formControlData,
      operacao: formControlOperacao
    }

    await $.ajax({
      url: serviceAccessRoute + contas,
      data: {
        inputItem: {
          tk: this.auth.getToken(),
          body: dados
        }
      },
      cache: false,
      type: 'POST',
      success: function (response) {
        ContasService.response = response;
      }
    });
    this.getTableView();
    this.jqueryTratament.validateResponse(ContasService.response);
    ContasService.response = undefined;
  }

  async updateConta(formGroupConta: FormGroup, id: Number) {
    let dadosUpdate = {
      id: id,
      descricao: formGroupConta.value.formControlDescricao,
      operacao: formGroupConta.value.formControlOperacao,
      data: formGroupConta.value.formControlData,
      valor: formGroupConta.value.formControlValor
    }

    await $.ajax({
      url: serviceAccessRoute + contas,
      data: {
        updateConta: {
          tk: this.auth.getToken(),
          body: dadosUpdate
        }
      },
      cache: false,
      type: 'POST',
      success: function (response) {
        ContasService.response = response;
      }
    });
    this.getTableView();
    this.jqueryTratament.validateResponse(ContasService.response);
    ContasService.response = undefined;
  }

  async buscaConta(valueSearch) {
    await $.ajax({
      url: serviceAccessRoute + contas,
      data: {
        buscaConta: {
          tk: this.auth.getToken(),
          body: valueSearch
        }
      },
      cache: false,
      type: 'GET',
      success: function (response) {
        ContasService.response = response;
      }
    });
    if (this.jqueryTratament.validateResponse(ContasService.response)) {
      ContasService.response = JSON.parse(ContasService.response);
      this.subject.next(ContasService.response);
    }
    ContasService.response = undefined;
  }

  async getContasForCaixa(action: string) {
    await $.ajax({
      url: serviceAccessRoute + contas,
      data: {
        getItemContasForCaixa: {
          tk: this.auth.getToken(),
          body: action
        }
      },
      cache: false,
      type: 'GET',
      success: function (response) {
        ContasService.response = response;
      }
    });
    if (this.jqueryTratament.validateResponse(ContasService.response)) {
      ContasService.response = JSON.parse(ContasService.response);
      this.subject.next(ContasService.response);
    }
    ContasService.response = null;

  }
}
