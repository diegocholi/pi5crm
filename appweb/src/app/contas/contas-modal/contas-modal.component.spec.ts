import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContasModalComponent } from './contas-modal.component';

describe('ContasModalComponent', () => {
  let component: ContasModalComponent;
  let fixture: ComponentFixture<ContasModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContasModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContasModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
