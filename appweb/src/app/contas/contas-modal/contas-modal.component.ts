import { Component, OnInit, Input } from '@angular/core';
import { ContasService } from '../contas.service'
import * as $ from 'jquery';

@Component({
  selector: 'appweb-contas-modal',
  templateUrl: './contas-modal.component.html',
  styleUrls: ['./contas-modal.component.css']
})
export class ContasModalComponent implements OnInit {
  /**
   * @modalTitle Titulo do modal
   */
  @Input()
  modalTitle: string = 'Title Default Modal';

  /**
   * @buttonModalName : Edita o nome do botão do motal
   */
  @Input()
  buttonModalName: string = 'Title Default Button Modal';

  /**
  * @buttonClassCss : Configuração da classe css botstrap
  */
  @Input()
  buttonClassCss = 'btn btn-success';

  constructor(private service: ContasService) { }

  ngOnInit() {
    $('#formControlOperacao').val('');
  }

  submitCadastrContas() {
    if (
      $('#formControlValor').val() &&
      $('#formControlDescricaoAdd').val() &&
      $('#formControlData').val() &&
      $('#formControlOperacao').val()
    ) {
      this.service.inserirConta(
        Number($('#formControlValor').val()),
        String($('#formControlDescricaoAdd').val()),
        String($('#formControlData').val()),
        String($('#formControlOperacao').val())
      );
      $('#formControlValor').val('');
      $('#formControlDescricaoAdd').val('');
      $('#formControlData').val('');
      $('#formControlOperacao').val('');

    } else {
      alert('Preencha todos os campos !')
    }

  }
}
