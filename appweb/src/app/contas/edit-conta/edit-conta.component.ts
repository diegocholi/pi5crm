import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ContasService } from '../contas.service';

@Component({
  selector: 'appweb-edit-conta',
  templateUrl: './edit-conta.component.html',
  styleUrls: ['./edit-conta.component.css']
})
export class EditContaComponent implements OnInit {
  formGroupConta: FormGroup;

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder,
    private service: ContasService, private router: Router) { }
  id: number;
  operacao: Number;
  descricao: string;
  data: Date;
  valor: Number;

  ngOnInit() {

    this.activatedRoute.params.subscribe(params => {
      if (params.opr === 'conta a pagar')
        this.operacao = 1;
      else
        this.operacao = 2;

      this.id = params.id;
      this.descricao = params.descricao;
      this.data = params.data;
      this.valor = params.valor;
    });

    this.formGroupConta = this.formBuilder.group({
      formControlOperacao: [this.operacao, Validators.required],
      formControlDescricao: [this.descricao, Validators.required],
      formControlData: [this.data, Validators.required],
      formControlValor: [this.valor, Validators.required]
    });
  }

  salvarEdicao() {
    this.service.updateConta(this.formGroupConta, this.id);
    this.router.navigate(['/conta-controle']);
  }

}
