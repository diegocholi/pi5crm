export const serviceAccessRoute = "http://localhost:8080/";
export const auth = "controller/auth.php";
export const fornecedorRoute = "controller/Provider.php";
export const fornecedorItensRoute = "controller/ItemProvider.php";
export const storageItensRoute = "controller/Storage.php";
export const contas = "controller/Contas.php";
export const fluxoBancario = "controller/FluxoBancario.php";
export const caixa = "controller/Caixa.php";