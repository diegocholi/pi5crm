import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../login/authService/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private authService: AuthService,
    private router: Router) { }

  canActivate(
    route: ActivatedRouteSnapshot, // Recebendo a rota em si
    state: RouterStateSnapshot // O estado da rota
  ): Observable<boolean> | boolean {

    if (this.authService.getStatusUser())
      return true;

    this.router.navigate(['/login']);
    return false;
  }


}
