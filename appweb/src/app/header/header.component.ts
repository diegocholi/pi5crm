import { Component, OnInit } from '@angular/core';
import { AuthService } from '../login/authService/auth.service';
@Component({
  selector: 'appweb-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  signOut() {
    this.authService.signOut();
  }
}
